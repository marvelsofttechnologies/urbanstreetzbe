#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:5.0-buster-slim AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:5.0-buster-slim AS build
WORKDIR /app
COPY ["UrbanStreetz/UrbanStreetz.csproj", "UrbanStreetz/"]
RUN dotnet restore "UrbanStreetz/UrbanStreetz.csproj"

COPY UrbanStreetz/. UrbanStreetz/
COPY UrbanStreetz/EmailTemplates/. UrbanStreetz/EmailTemplates

WORKDIR "/app/UrbanStreetz"
RUN dotnet publish -c Release -o out

FROM base AS final
WORKDIR /app
COPY --from=build /app/UrbanStreetz/out ./
CMD ASPNETCORE_URLS=http://*:$PORT dotnet UrbanStreetz.dll