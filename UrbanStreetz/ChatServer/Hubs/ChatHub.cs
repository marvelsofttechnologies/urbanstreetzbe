using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UrbanStreetz.Models.AppModels;
using UrbanStreetz.Models.InputModels;
using UrbanStreetz.Models.ViewModels;
using UrbanStreetz.Repositories.Interfaces;
using UrbanStreetz.Utilities.Extentions;

namespace UrbanStreetz.ChatServer.Hubs {
    [Authorize]
    public class ChatHub : Hub {
        private readonly IGroupRepository _groupRepository;
        private readonly IUserRepository _userRepository;
        private readonly IMessageRepository _messageRepository;
        private readonly IMapper _mapper;
        private readonly IMediaRepository _mediaRepository;

        public ChatHub(IGroupRepository groupRepository, IUserRepository userRepository, IMessageRepository messageRepository, IMapper mapper, IMediaRepository mediaRepository) {
            _groupRepository = groupRepository;
            _userRepository = userRepository;
            _messageRepository = messageRepository;
            _mapper = mapper;
            _mediaRepository = mediaRepository;
        }

        public async Task SendMessage(MessageModel message) {
            // get receiver
            var receiver = _userRepository.ListUsers().Result.Users.FirstOrDefault(u => u.Id == message.RecipientId);

            if (receiver != null) {
                // save message
                var mappedMessage = _mapper.Map<Message>(message);
                mappedMessage.SenderId = _userRepository.LoggedInUser().Id;
                var savedMessage = _messageRepository.CreateAndReturn(mappedMessage);

                if (savedMessage != null) {
                    // save image 
                    var mappedMedia = _mapper.Map<Media>(message.Media);
                    mappedMedia.MessageId = savedMessage.Id;
                    await _mediaRepository.UploadMedia(mappedMedia);

                    // send message to recipient
                    await Clients.Group(receiver.Id.ToString())
                                 .SendAsync("ReceiveMessage", _mapper.Map<MessageView>(savedMessage));
                }
            }
        }

        public async Task SendGroupMessage(MessageModel message) {
            // get group
            var group = _groupRepository.GetAll().FirstOrDefault(group => group.Id == message.GroupId);

            if (group != null) {
                // save message
                var mappedMessage = _mapper.Map<Message>(message);
                mappedMessage.SenderId = _userRepository.LoggedInUser().Id;
                var savedMessage = _messageRepository.CreateAndReturn(mappedMessage);

                if (savedMessage != null) {
                    // save image 
                    var mappedMedia = _mapper.Map<Media>(message.Media);
                    mappedMedia.MessageId = savedMessage.Id;
                    await _mediaRepository.UploadMedia(mappedMedia);

                    // send message to group
                    var mappedResponseMessage = _mapper.Map<MessageView>(savedMessage);
                    mappedResponseMessage.GroupName = group.Name;
                    await Clients.Group(FormatGroupName(group.Name)).SendAsync("ReceiveGroupMessage", mappedResponseMessage);
                }
            }
        }

        public override async Task OnConnectedAsync( ) {
            // add device to user's private group
            await Groups.AddToGroupAsync(Context.ConnectionId, Context.User.Identity.Name);

            // add device to each group for user
            foreach (string name in GetUserGroupNames())
                await Groups.AddToGroupAsync(Context.ConnectionId, FormatGroupName(name));

            await base.OnConnectedAsync();
        }

        public override async Task OnDisconnectedAsync(Exception? ex) {
            // remove device from user's private group
            await Groups.RemoveFromGroupAsync(Context.ConnectionId, Context.User.Identity.Name);

            // remove device to each group for user
            foreach (string name in GetUserGroupNames())
                await Groups.RemoveFromGroupAsync(Context.ConnectionId, FormatGroupName(name));

            await base.OnDisconnectedAsync(ex);
        }

        private IEnumerable<string> GetUserGroupNames( ) {
            int.TryParse(Context.User.Identity.Name, out int loggedInUserId);

            var userGroupNames = _groupRepository.AllGroups()
                .Where(group => group.Users.Any(groupUser => groupUser.UserId == loggedInUserId))
                .Select(group => group.Name);

            return userGroupNames;
        }

        private string FormatGroupName(string groupName)
            => groupName.Trim().ToLower().Replace(" ", "-");
    }
}