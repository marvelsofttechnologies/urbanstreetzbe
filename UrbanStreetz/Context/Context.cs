﻿using System;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using UrbanStreetz.Models.AppModels;
using UrbanStreetz.Models.ChatModels;
using UrbanStreetz.Models.IdentityModels;

namespace UrbanStreetz.Context
{
    public class AppDbContext : IdentityDbContext<User,Role,int>
    {
        public AppDbContext(DbContextOptions<AppDbContext> options)
            : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<ChatUser>().HasKey(cu => new { cu.UserId, cu.ChatId });

            modelBuilder.Entity<User>().HasOne(u => u.ProfilePicture).WithOne(m => m.User).HasForeignKey<Media>(m => m.UserId);
            modelBuilder.Entity<User>().HasMany(u => u.Followers).WithOne(f => f.User).HasForeignKey(ur => ur.UserId).OnDelete(DeleteBehavior.NoAction);
            modelBuilder.Entity<User>().HasMany(u => u.Following).WithOne(f => f.Follower).HasForeignKey(ur => ur.FollowerId).OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<User>().ToTable("Users");
            modelBuilder.Entity<Role>().ToTable("Roles");
            modelBuilder.Entity<IdentityUserClaim<int>>().ToTable("UserClaims");
            modelBuilder.Entity<IdentityUserRole<int>>().ToTable("UserRoles");
            modelBuilder.Entity<IdentityUserLogin<int>>().ToTable("UserLogins");
            modelBuilder.Entity<IdentityRoleClaim<int>>().ToTable("RoleClaims");
            modelBuilder.Entity<IdentityUserToken<int>>().ToTable("UserTokens");
        }

        public DbSet<City> Cities { get; set; }
        public DbSet<Code> Codes { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<Gender> Genders { get; set; }
        public DbSet<LocationType> LocationTypes { get; set; }
        public DbSet<Media> Media { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<PostCategory> PostCategories { get; set; }
        public DbSet<Report> Reports { get; set; }
        public DbSet<State> States {get;set;}
        public DbSet<Status> Statuses { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<UserInterest> UserInterests { get; set; }
        public DbSet<UserFollowers> UserFollowers { get; set; }
        public DbSet<Like> Likes { get; set; }
        public DbSet<Story> Stories { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<GroupUser> GroupUsers { get; set; }
        public DbSet<Chat> Chats { get; set;}
        public DbSet<ChatUser> ChatUsers { get; set; }
        public DbSet<Highlight> Highlights { get; set; }
        public DbSet<Cart> Carts { get; set; }
    }
}

