﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

using UrbanStreetz.Models.InputModels;
using UrbanStreetz.Models.UtilityModels;
using UrbanStreetz.Models.ViewModels;
using UrbanStreetz.Services;
using UrbanStreetz.Services.Interfaces;
using UrbanStreetz.Utilities;

namespace UrbanStreetz.Controllers {
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    public class CartController : Controller {
        private readonly ICartService _cartService;
        private readonly PagingOptions _defaultPagingOptions;

        public CartController(ICartService cartService, IOptions<PagingOptions> defaultPagingOptions) {
            _cartService = cartService;
            _defaultPagingOptions = defaultPagingOptions.Value;
        }


        [HttpPost("create", Name = nameof(CreateCart))]
        [ProducesResponseType(typeof(StandardResponse<CartView>), 200)]
        public ActionResult<StandardResponse<CartView>> CreateCart([FromBody] CartModel cart) {
            var result = _cartService.CreateCart(cart);
            return Ok(result);
        }

        [HttpDelete("delete/{id}", Name = nameof(DeleteCart))]
        [ProducesResponseType(typeof(StandardResponse<bool>), 200)]
        public ActionResult<StandardResponse<bool>> DeleteCart(int id) {
            var result = _cartService.RemoveCart(id);
            return Ok(result);
        }

        [HttpGet("list", Name = nameof(ListCart))]
        [ProducesResponseType(typeof(PagedCollection<CartView>), 200)]
        public ActionResult<StandardResponse<PagedCollection<CartView>>> ListCart([FromQuery]PagingOptions pagingOptions)
        {
            pagingOptions.Replace(_defaultPagingOptions);
            var carts = _cartService.ListCart(pagingOptions);
            return Ok(carts);
        }
    }
}
