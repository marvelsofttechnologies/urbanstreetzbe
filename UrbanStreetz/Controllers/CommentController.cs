using System.Collections;
using System.Collections.Generic;
using Google.Apis.Util;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using UrbanStreetz.Models.InputModels;
using UrbanStreetz.Models.UtilityModels;
using UrbanStreetz.Models.ViewModels;
using UrbanStreetz.Services;
using UrbanStreetz.Services.Interfaces;

namespace UrbanStreetz.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CommentController : Controller
    {
        private readonly ICommentService _commentService;
        private readonly PagingOptions _defaultPagingOptions;

        public CommentController(ICommentService commentService, IOptions<PagingOptions> defaultPagingOptions)
        {
            _commentService = commentService;
            _defaultPagingOptions = defaultPagingOptions.Value;
        }

        [HttpPost("new", Name = nameof(NewComment))]
        [ProducesResponseType(200)]
        public ActionResult<StandardResponse<CommentView>> NewComment(CommentModel model)
        {
            return Ok(_commentService.New(model));
        }

        [HttpGet("comments/{postId}", Name = nameof(ListPostsComments))]
        [ProducesResponseType(200)]
        public ActionResult<StandardResponse<PagedCollection<CommentView>>> ListPostsComments(int postId, [FromQuery]PagingOptions pagingOptions)
        {
            pagingOptions.Replace(_defaultPagingOptions);
            return Ok(_commentService.ListPostComments(postId, pagingOptions));
        }

        
        [HttpGet("{commentId}/users", Name = nameof(ListCommentLikeUsers))]
        [ProducesResponseType(typeof(StandardResponse<PagedCollection<PostView>>), 200)]
        public ActionResult<StandardResponse<PagedCollection<PostView>>> ListCommentLikeUsers(int commentId, [FromQuery] PagingOptions pagingOptions){
            pagingOptions.Replace(_defaultPagingOptions);
            return Ok(_commentService.ListCommentLikeUsers(commentId, pagingOptions));
        }

    }
}