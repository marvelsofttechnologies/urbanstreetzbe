using Google.Apis.Util;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Threading.Tasks;
using UrbanStreetz.Models.InputModels;
using UrbanStreetz.Models.UtilityModels;
using UrbanStreetz.Models.ViewModels;
using UrbanStreetz.Services;
using UrbanStreetz.Services.Interfaces;

namespace UrbanStreetz.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Authorize]
    public class GroupController : Controller
    {
        private readonly IGroupService _groupService;
        private readonly PagingOptions _defaultPagingOptions;

        public GroupController(IGroupService groupService, IOptions<PagingOptions> defaultPagingOptions)
        {
            _groupService = groupService;
            _defaultPagingOptions = defaultPagingOptions.Value;
        }

        [HttpPost("create", Name = nameof(CreateGroup))]
        [ProducesResponseType(typeof(StandardResponse<GroupView>), 200)]
        public ActionResult<StandardResponse<GroupView>> CreateGroup([FromBody] GroupModel group)
{
            var result = _groupService.CreateGroup(group);
            return Ok(result);
        }

        [HttpPut("update", Name = nameof(UpdateGroup))]
        [ProducesResponseType(typeof(StandardResponse<GroupView>), 200)]
        public ActionResult<StandardResponse<GroupView>> UpdateGroup([FromBody] GroupModel group)
        {
            var result = _groupService.UpdateGroup(group);
            return Ok(result);
        }
        
        [HttpDelete("delete/{id}", Name = nameof(DeleteGroup))]
        [ProducesResponseType(typeof(StandardResponse<GroupView>), 200)]
        public ActionResult<StandardResponse<GroupView>> DeleteGroup(int id)
        {
            var result = _groupService.DeleteGroup(id);
            return Ok(result);
        }

        [HttpGet("list/user/{id}", Name = nameof(UserGroups))]
        [ProducesResponseType(typeof(StandardResponse<PagedCollection<GroupView>>), 200)]
        public ActionResult<PagedCollection<GroupView>> UserGroups(int id, [FromQuery] PagingOptions pagingOptions)
        {
            pagingOptions.Replace(_defaultPagingOptions);
            return Ok(_groupService.AllUserGroups(id, pagingOptions));
        }

        [HttpGet("list", Name = nameof(AllGroups))]
        [ProducesResponseType(typeof(StandardResponse<PagedCollection<GroupView>>), 200)]
        public ActionResult<PagedCollection<GroupView>> AllGroups([FromQuery] PagingOptions pagingOptions)
        {
            pagingOptions.Replace(_defaultPagingOptions);
            return Ok(_groupService.AllGroups(pagingOptions));
        }


    }
}