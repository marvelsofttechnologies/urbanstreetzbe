using Google.Apis.Util;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Threading.Tasks;
using UrbanStreetz.Models.InputModels;
using UrbanStreetz.Models.UtilityModels;
using UrbanStreetz.Models.ViewModels;
using UrbanStreetz.Services.Interfaces;

namespace UrbanStreetz.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Authorize]
    public class HighlightController : Controller
    {
        private readonly IHighlightService _highlightService;
        private readonly PagingOptions _defaultPagingOptions;

        public HighlightController(IHighlightService highlightService, IOptions<PagingOptions> defaultPagingOptions)
        {
            _highlightService = highlightService;
            _defaultPagingOptions = defaultPagingOptions.Value;
        }

        [HttpPost("create", Name = nameof(AddStoryToHighlight))]
        [ProducesResponseType(typeof(StandardResponse<HighlightView>), 200)]
        public ActionResult<StandardResponse<HighlightView>> AddStoryToHighlight([FromBody] HighlightModel highlight)
        {
            var result = _highlightService.AddStoryToHighlight(highlight);
            return Ok(result);
        }

        [HttpGet("list", Name = nameof(ListHighlights))]
        [ProducesResponseType(typeof(PagedCollection<HighlightView>), 200)]
        public ActionResult<StandardResponse<PagedCollection<HighlightView>>> ListHighlights([FromQuery] PagingOptions pagingOptions)
        {
            pagingOptions.Replace(_defaultPagingOptions);
            var stories = _highlightService.ListHighlights(pagingOptions);
            return Ok(stories);
        }

        [HttpGet("list/{hightlightId}/stories", Name = nameof(ListHighlightStories))]
        [ProducesResponseType(typeof(PagedCollection<HighlightView>), 200)]
        public ActionResult<StandardResponse<PagedCollection<HighlightView>>> ListHighlightStories(int hightlightId, [FromQuery] PagingOptions pagingOptions)
        {
            pagingOptions.Replace(_defaultPagingOptions);
            var stories = _highlightService.ListHighlightStories(hightlightId, pagingOptions);
            return Ok(stories);
        }

        [HttpDelete("delete/story/{id}", Name = nameof(DeleteHighlightStory))]
        [ProducesResponseType(typeof(string), 200)]
        public ActionResult<StandardResponse<bool>> DeleteHighlightStory(int id)
            => Ok(_highlightService.DeleteHighlightStory(id));

        [HttpDelete("delete/{id}", Name = nameof(DeleteHighlight))]
        [ProducesResponseType(typeof(string), 200)]
        public ActionResult<StandardResponse<bool>> DeleteHighlight(int id)
            => Ok(_highlightService.DeleteHighlight(id));
    }
}