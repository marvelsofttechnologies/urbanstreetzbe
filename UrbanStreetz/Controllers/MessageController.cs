using Google.Apis.Util;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Threading.Tasks;
using UrbanStreetz.Models.InputModels;
using UrbanStreetz.Models.UtilityModels;
using UrbanStreetz.Models.ViewModels;
using UrbanStreetz.Services;
using UrbanStreetz.Services.Interfaces;

namespace UrbanStreetz.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Authorize]
    public class MessageController : Controller
    {
        private readonly IMessageService _messageService;
        private readonly PagingOptions _defaultPagingOptions;

        public MessageController(IMessageService messageService, IOptions<PagingOptions> defaultPagingOptions)
        {
            _messageService = messageService;
            _defaultPagingOptions = defaultPagingOptions.Value;
        }

        [HttpDelete("{id}", Name = nameof(DeleteMessage))]
        [ProducesResponseType(typeof(PagedCollection<PostCategoryView>), 200)]
        public ActionResult<StandardResponse<PagedCollection<PostCategoryView>>> DeleteMessage(int id)
        {
            var result = _messageService.DeleteMessage(id);
            return Ok(result);
        }

        [HttpGet("overview", Name = nameof(MessageOverview))]
        [ProducesResponseType(typeof(PagedCollection<PostCategoryView>), 200)]
        public ActionResult<StandardResponse<PagedCollection<PostCategoryView>>> MessageOverview([FromQuery] PagingOptions pagingOptions)
        {
            pagingOptions.Replace(_defaultPagingOptions);
            var messages = _messageService.MessageOverview(pagingOptions);
            return Ok(messages);
        }

        [HttpGet("list/chat/{reciepientId}", Name = nameof(ChatMessages))]
        [ProducesResponseType(typeof(PagedCollection<PostCategoryView>), 200)]
        public ActionResult<StandardResponse<PagedCollection<PostCategoryView>>> ChatMessages(int reciepientId, [FromQuery] PagingOptions pagingOptions)
        {
            pagingOptions.Replace(_defaultPagingOptions);
            var messages = _messageService.ChatMessages(reciepientId, pagingOptions);
            return Ok(messages);
        }

        [HttpGet("list/group/{groupId}", Name = nameof(GroupMessages))]
        [ProducesResponseType(typeof(PagedCollection<PostCategoryView>), 200)]
        public ActionResult<StandardResponse<PagedCollection<PostCategoryView>>> GroupMessages(int groupId, [FromQuery] PagingOptions pagingOptions)
        {
            pagingOptions.Replace(_defaultPagingOptions);
            var messages = _messageService.GroupMessages(groupId, pagingOptions);
            return Ok(messages);
        }
    }
}