
using Google.Apis.Util;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using UrbanStreetz.Models.InputModels;
using UrbanStreetz.Models.UtilityModels;
using UrbanStreetz.Models.ViewModels;
using UrbanStreetz.Services.Interfaces;

namespace UrbanStreetz.Controllers {
    [ApiController]
    [Route("[controller]")]
    [Authorize]
    public class PostController : Controller
    {
        private readonly IPostService _postService;
        private readonly PagingOptions _defaultPagingOptions;

        public PostController(IPostService postService, IOptions<PagingOptions> defaultPagingOptions)
        {
            _postService = postService;
            _defaultPagingOptions = defaultPagingOptions.Value;
        }

        [HttpGet("categories", Name = nameof(ListPostCategories))]
        [ProducesResponseType(typeof(PagedCollection<PostCategoryView>), 200)]
        public ActionResult<StandardResponse<PagedCollection<PostCategoryView>>> ListPostCategories([FromQuery] PagingOptions pagingOptions)
        {
            pagingOptions.Replace(_defaultPagingOptions);
            var categories = _postService.ListPostCategories(pagingOptions);
            return Ok(categories);
        }

        [HttpPost("create", Name = nameof(CreatePost))]
        [ProducesResponseType(typeof(StandardResponse<PostView>), 200)]
        public ActionResult<StandardResponse<PostView>> CreatePost([FromBody] PostModel post)
        {
                var result = _postService.CreatePost(post);
            return Ok(result);
        }
        
        [HttpGet("{postId}", Name = nameof(PostDetail))]
        [ProducesResponseType(typeof(PostView), 200)]
        public ActionResult<PostView> PostDetail(int postId)
            => Ok(_postService.GetPost(postId));

        [HttpGet("list", Name = nameof(ListPosts))]
        [ProducesResponseType(typeof(PagedCollection<PostView>), 200)]
        public ActionResult<StandardResponse<PagedCollection<PostView>>> ListPosts(string searchQuery, [FromQuery]PagingOptions pagingOptions)
        {
            pagingOptions.Replace(_defaultPagingOptions);
            var posts = _postService.ListPosts(searchQuery, pagingOptions);
            return Ok(posts);
        }

        [HttpGet("list/interests", Name = nameof(ListInterestedPosts))]
        [ProducesResponseType(typeof(PagedCollection<PostView>), 200)]
        public ActionResult<StandardResponse<PagedCollection<PostView>>> ListInterestedPosts(string searchQuery, [FromQuery]PagingOptions pagingOptions){
            pagingOptions.Replace(_defaultPagingOptions);
            return Ok(_postService.ListPersonalizedPosts(searchQuery, pagingOptions));
        }

        [HttpGet("list/{postId}/related", Name = nameof(ListRelatedPosts))]
        [ProducesResponseType(typeof(StandardResponse<PagedCollection<PostView>>), 200)]
        public ActionResult<StandardResponse<PagedCollection<PostView>>> ListRelatedPosts(int postId, [FromQuery]PagingOptions pagingOptions) {
            pagingOptions.Replace(_defaultPagingOptions);
            var posts = _postService.RelatedPosts(postId, pagingOptions);

            return Ok(posts);
        }

        [HttpGet("list/popular", Name = nameof(ListPopularPosts))]
        [ProducesResponseType(typeof(StandardResponse<PagedCollection<PostView>>), 200)]
        public ActionResult<StandardResponse<PagedCollection<PostView>>> ListPopularPosts(string searchQuery, [FromQuery] PagingOptions pagingOptions){
            pagingOptions.Replace(_defaultPagingOptions);
            return Ok(_postService.PopularPosts(searchQuery, pagingOptions));
        }

        [HttpPost("like/{postId}", Name = nameof(Like))]
        [ProducesResponseType(typeof(StandardResponse<bool>), 200)]
        public ActionResult<StandardResponse<bool>> Like(int postId)
        {
            var result = _postService.Like(postId);
            return Ok(result);
        }

        [HttpPost("dislike{postId}", Name = nameof(Dislike))]
        [ProducesResponseType(typeof(StandardResponse<bool>), 200)]
        public ActionResult<StandardResponse<bool>> Dislike(int postId)
        {
            var result = _postService.Dislike(postId);
            return Ok(result);
        }

        
        [HttpGet("like/{postId}/users", Name = nameof(ListPostLikeUsers))]
        [ProducesResponseType(typeof(StandardResponse<PagedCollection<PostView>>), 200)]
        public ActionResult<StandardResponse<PagedCollection<PostView>>> ListPostLikeUsers(int postId, [FromQuery]PagingOptions pagingOptions){
            pagingOptions.Replace(_defaultPagingOptions);
            return Ok(_postService.ListPostLikeUsers(postId, pagingOptions));
    }
    }
}