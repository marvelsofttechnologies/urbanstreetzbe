using Google.Apis.Util;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Threading.Tasks;
using UrbanStreetz.Models.InputModels;
using UrbanStreetz.Models.UtilityModels;
using UrbanStreetz.Models.ViewModels;
using UrbanStreetz.Services.Interfaces;

namespace UrbanStreetz.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Authorize]
    public class StoryController : Controller
    {
        private readonly IStoryService _storyService;
        private readonly PagingOptions _defaultPagingOptions;

        public StoryController(IStoryService storyService, IOptions<PagingOptions> defaultPagingOptions)
        {
            _storyService = storyService;
            _defaultPagingOptions = defaultPagingOptions.Value;
        }

        [HttpPost("create", Name = nameof(CreateStory))]
        [ProducesResponseType(typeof(StandardResponse<StoryView>), 200)]
        public async Task<ActionResult<StandardResponse<StoryView>>> CreateStory([FromBody] StoryModel story)
        {
            var result = await _storyService.CreateStory(story);
            return Ok(result);
        }

        [HttpGet ("{id}", Name = nameof(GetStory))]
        [ProducesResponseType(typeof(StandardResponse<StoryView>), 200)]
        public ActionResult<StandardResponse<StoryView>> GetStory(int id)
        {
            var story = _storyService.GetStory(id);
            return Ok(story);
        }

        [HttpGet("list", Name = nameof(ListUserStories))]
        [ProducesResponseType(typeof(PagedCollection<StoryView>), 200)]
        public ActionResult<StandardResponse<PagedCollection<StoryView>>> ListUserStories([FromQuery] PagingOptions pagingOptions)
        {
            pagingOptions.Replace(_defaultPagingOptions);
            var stories = _storyService.ListUserStories(pagingOptions);
            return Ok(stories);
        }

        [HttpGet("list/followers", Name = nameof(ListFollowersStories))]
        [ProducesResponseType(typeof(PagedCollection<StoryGroupView>), 200)]
        public ActionResult<StandardResponse<PagedCollection<StoryGroupView>>> ListFollowersStories([FromQuery] PagingOptions pagingOptions)
        {
            pagingOptions.Replace(_defaultPagingOptions);
            var stories = _storyService.ListFollowerStories(pagingOptions);
            return Ok(stories);
        }

        [HttpDelete("delete/{id}", Name = nameof(DeleteStory))]
        [ProducesResponseType(typeof(string), 200)]
        public ActionResult<StandardResponse<string>> DeleteStory(int id)
        {
            return Ok(_storyService.DeleteStory(id)); 
        }
    }
}