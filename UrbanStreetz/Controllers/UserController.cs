using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System.Net.NetworkInformation;
using UrbanStreetz.Models.InputModels;
using UrbanStreetz.Models.UtilityModels;
using UrbanStreetz.Models.ViewModels;
using UrbanStreetz.Services.Abstractions;
using UrbanStreetz.Utilities;

namespace UrbanStreetz.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UserController : Controller
    {
        private readonly IUserService _userService;
        private readonly PagingOptions _defaultPagingOptions;

        public UserController(IUserService userService, IOptions<PagingOptions> defaultPagingOptions)
        {
            _userService = userService;
            _defaultPagingOptions = defaultPagingOptions.Value;
        }

        [HttpPost("create", Name = nameof(CreateUser))]
        [ProducesResponseType(typeof(StandardResponse<UserView>), 200)]
        public ActionResult<StandardResponse<UserView>> CreateUser(UserModel model)
        {
            return Ok(_userService.CreateUser(model));
        }

        [HttpPost("login", Name = nameof(LoginUser))]
        [ProducesResponseType(typeof(StandardResponse<UserView>), 200)]
        public ActionResult<StandardResponse<UserView>> LoginUser(LoginModel model)
        {
            return Ok(_userService.Authenticate(model));
        }
        
        [HttpGet("verifyUser/{token}/{email}", Name = nameof(Verify))]
        [ProducesResponseType(200)]
        public ActionResult<StandardResponse<UserView>> Verify(string token, string email)
        {
            return Ok(_userService.VerifyUser(token, email));
        }

        [HttpGet("reset/initiate/{email}", Name = nameof(InitiateReset))]
        [ProducesResponseType(200)]
        public ActionResult<StandardResponse<UserView>> InitiateReset(string email)
        {
            return Ok(_userService.InitiatePasswordReset(email));
        }

        [HttpPost("reset/complete", Name = nameof(CompleteReset))]
        [ProducesResponseType(200)]
        public ActionResult<StandardResponse<UserView>> CompleteReset(PasswordReset payload)
        {
            return Ok(_userService.CompletePasswordReset(payload));
        }

        [HttpPut("update", Name = nameof(UpdateUser))]
        [Authorize]
        [ProducesResponseType(200)]
        public ActionResult<StandardResponse<UserView>> UpdateUser(UpdateUserModel model)
        {
            return Ok(_userService.UpdateUser(model));
        }

        [HttpPost("follow/{userId}", Name = nameof(Follow))]
        [Authorize]
        [ProducesResponseType(200)]
        public ActionResult<StandardResponse<UserView>> Follow(int userId)
        {
            return Ok(_userService.Follow(userId));
        }

        [HttpDelete("unfollow/{userId}", Name = nameof(Unfollow))]
        [Authorize]
        [ProducesResponseType(200)]
        public ActionResult<StandardResponse<UserView>> Unfollow(int userId)
        {
            return Ok(_userService.UnFollow(userId));
        }

        [HttpGet("user-profile/{userId}", Name = nameof(UserProfile))]
        [Authorize]
        [ProducesResponseType(200)]
        public ActionResult<StandardResponse<UserProfileView>> UserProfile(int userId) {
            return Ok(_userService.UserProfile(userId));
        }

        [HttpGet("followers", Name = nameof(ListFollowers))]
        [Authorize]
        [ProducesResponseType(200)]
        public ActionResult<StandardResponse<PagedCollection<UserView>>> ListFollowers([FromQuery] PagingOptions pagingOptions){
            pagingOptions.Replace(_defaultPagingOptions);
            return Ok(_userService.ListMyFollowers(pagingOptions));
        }
    }
}