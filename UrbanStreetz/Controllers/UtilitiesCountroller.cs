using System.Collections;
using System.Collections.Generic;
using Google.Apis.Util;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using UrbanStreetz.Models.UtilityModels;
using UrbanStreetz.Models.ViewModels;
using UrbanStreetz.Services.Interfaces;

namespace UrbanStreetz.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UtilitiesController : Controller
    {
        private readonly ICountryService _countryService;
        private readonly PagingOptions _defaultPagingOptions;

        public UtilitiesController(ICountryService countryService, IOptions<PagingOptions> defaultPagingOptions)
        {
            _countryService = countryService;
            _defaultPagingOptions = defaultPagingOptions.Value;
        }

        [HttpGet("countries", Name = nameof(ListCountries))]
        [ProducesResponseType(typeof(PagedCollection<CountryView>), 200)]
        public ActionResult<StandardResponse<PagedCollection<CountryView>>> ListCountries([FromQuery] PagingOptions pagingOptions)
        {
            pagingOptions.Replace(_defaultPagingOptions);
            var countries = _countryService.ListCountries(pagingOptions);
            return Ok(countries);
        }

        [HttpGet("states/{countryId}", Name = nameof(ListStates))]
        [ProducesResponseType(typeof(PagedCollection<StateView>), 200)]
        public ActionResult<StandardResponse<PagedCollection<StateView>>> ListStates(int countryId, [FromQuery] PagingOptions pagingOptions)
        {
            pagingOptions.Replace(_defaultPagingOptions);
            var states = _countryService.ListStates(countryId, pagingOptions);
            return Ok(states);
        }

        [HttpGet("cities/{stateId}", Name = nameof(ListCities))]
        [ProducesResponseType(typeof(PagedCollection<BaseNameModelView>), 200)]
        public ActionResult<StandardResponse<PagedCollection<BaseNameModelView>>> ListCities(int stateId, [FromQuery] PagingOptions pagingOptions)
        {
            pagingOptions.Replace(_defaultPagingOptions);
            var cities = _countryService.ListCities(stateId, pagingOptions);
            return Ok(cities);
        }
    }
}