using UrbanStreetz.EmailServer;
using UrbanStreetz.Utilities.Abstrctions;
using SendGrid;

namespace UrbanStreetz.Utilities
{
    public class BaseEmailHandler
    {
        public static IBaseEmailHandler Build(EmailHandlerTypeEnum type,Globals globals,ISendGridClient _sendGridClient)
        {
            switch (type)
            {
                case EmailHandlerTypeEnum.SENDGRID:
                    return new SendGridEmailHandler(globals,_sendGridClient);

                case EmailHandlerTypeEnum.MAILGUN:
                    return new MailGunEmailHandler(globals);

                default:
                    return new MailGunEmailHandler(globals);
            }
        }
        public enum EmailHandlerTypeEnum {
            SENDGRID = 1,
            MAILGUN
        }
    }
}