using System.Threading.Tasks;

namespace UrbanStreetz.Utilities.Abstrctions
{
    public interface IBaseEmailHandler
    {
        Task<bool> SendEmail(string email, string subject, string message);
    }
}