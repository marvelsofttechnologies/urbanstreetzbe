using System.Net;
using System.Threading.Tasks;
using UrbanStreetz.Utilities;
using UrbanStreetz.Utilities.Abstrctions;
using RestSharp;
using RestSharp.Authenticators;

namespace UrbanStreetz.EmailServer
{
    public class MailGunEmailHandler : IBaseEmailHandler
    {
        public Globals _globals;
        public MailGunEmailHandler(Globals globals)
        {
            _globals = globals;
        }

        public async Task<bool> SendEmail(string email, string subject, string message)
        {
            try
            {
                var restClient = new RestClient(_globals.MailGunBaseUrl);
                restClient.Authenticator = new HttpBasicAuthenticator("api", _globals.MailGunApiKey);

                var restRequest = new RestRequest();
                restRequest.Resource = "messages";
                restRequest.AddParameter("from", "Alvin Grey <mailgun@alvingrey.com>");
                restRequest.AddParameter("to", email);
                restRequest.AddParameter("subject", subject);
                restRequest.AddParameter("html", message);
                restRequest.Method = Method.POST;
                var restResponse = restClient.Execute(restRequest);

                if (restResponse.StatusCode != HttpStatusCode.OK)
                    return false;

                return true;
            }
            catch (System.Exception)
            {
                return false;
            }
        }
    }
}