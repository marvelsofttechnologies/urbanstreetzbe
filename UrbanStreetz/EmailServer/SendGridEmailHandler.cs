using System;
using System.Threading.Tasks;
using UrbanStreetz.Utilities;
using UrbanStreetz.Utilities.Abstrctions;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace UrbanStreetz.EmailServer
{
    public class SendGridEmailHandler : IBaseEmailHandler
    {
        private Globals _globals;
        private readonly ISendGridClient _sendGridClient;
        public SendGridEmailHandler(Globals globals, ISendGridClient sendGridClient)
        {
            _globals = globals;
            _sendGridClient = sendGridClient;
        }

        public async Task<bool> SendEmail(string email, string subject, string message)
        {
            try
            {
               
                
                var SendGridMessage = new SendGridMessage
                {
                    From = new EmailAddress(_globals.SenderEmail,_globals.SendersName),
                    
                    Subject = subject,
                };

                SendGridMessage.AddTo(new EmailAddress(email));

                SendGridMessage.AddContent(MimeType.Html, message);

                var Response = await _sendGridClient.SendEmailAsync(SendGridMessage).ConfigureAwait(false);
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e);
                return false;
            }
        }
    }
}