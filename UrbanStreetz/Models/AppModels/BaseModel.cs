﻿using System;
namespace UrbanStreetz.Models.AppModels
{
    public class BaseModel
    {
        public BaseModel()
        {
        }

        public int Id { get; set; }
        public DateTime DateCreated { get; set; } = DateTime.Now;
        public DateTime DateModified { get; set; } = DateTime.Now;
    }
}

