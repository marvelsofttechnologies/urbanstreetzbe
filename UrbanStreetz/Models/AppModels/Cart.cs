﻿using UrbanStreetz.Models.IdentityModels;

namespace UrbanStreetz.Models.AppModels {
    public class Cart : BaseModel {
        public int PostId { get; set; }
        public Post Post { get; set; }
        public int CreatorUserId { get; set; }
        public User CreatorUser { get; set; }
        public int Quantity { get; set; }
    }
}
