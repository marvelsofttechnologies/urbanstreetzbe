﻿using System;
namespace UrbanStreetz.Models.AppModels
{
    public class City
    {
        public City()
        {
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public int StateId { get; set; }
    }
}

