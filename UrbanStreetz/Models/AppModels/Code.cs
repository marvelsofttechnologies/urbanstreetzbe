using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using UrbanStreetz.Models.IdentityModels;

namespace UrbanStreetz.Models.AppModels
{
    public class Code : BaseModel
    {
        [Required]
        [MaxLength(50)]
        [Column(TypeName = "varchar")]
        public string CodeString { get; set; }
        [MaxLength(50)]
        [Column(TypeName = "varchar")]
        public string Key { get; set; }
        public int? UserId { get; set; }
        public User User { get; set; }
        public DateTime ExpiryDate { get; set; }
        public bool IsExpired { get; set; }
        public string Token { get; set; }
    }
}