﻿using System.Collections.Generic;
using UrbanStreetz.Models.IdentityModels;

namespace UrbanStreetz.Models.AppModels
{
    public class Comment : BaseModel
    {
        public int PostId { get; set; }
        public int? UserId { get; set; }
        public int? StatusId { get; set; }
        public Post Post { get; set; }
        public User User { get; set; }
        public Status Status { get; set; }
        public string CommentText { get; set; }
        public int? CommentId { get; set; }
        public IEnumerable<Like> Likes { get; set; }
        public IEnumerable<Comment> Comments { get; set; }
        public IEnumerable<Report> Reports { get; set; }
    }
}