﻿using System;
using System.Collections.Generic;

namespace UrbanStreetz.Models.AppModels
{
    public class Country 
    {
        public Country()
        {
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public IEnumerable<State> States { get; set; }
    }
}

