﻿using System;
namespace UrbanStreetz.Models.AppModels
{
    public class Gender : BaseModel
    {
        public Gender()
        {
        }

        public string Name { get; set; }
    }

    public enum Genders
    {
        MALE = 1,
        FEMALE,
    }
}

