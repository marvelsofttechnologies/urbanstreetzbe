﻿using System.Collections.Generic;
using UrbanStreetz.Models.IdentityModels;

namespace UrbanStreetz.Models.AppModels
{
    public class Group : BaseModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int CreatorUserId { get; set; }
        public bool IsDeleted { get; set; } = false;
        public User CreatorUser { get; set; }
        public IEnumerable<GroupUser> Users { get; set; }
    }
}
