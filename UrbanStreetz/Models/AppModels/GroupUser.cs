﻿using UrbanStreetz.Models.IdentityModels;

namespace UrbanStreetz.Models.AppModels
{
    public class GroupUser : BaseModel
    {
        public int GroupId { get; set; }
        public int? UserId { get; set; }
        public bool IsAdmin { get; set; }
        public Group Group { get; set; }
        public User User { get; set; }
    }
}
