﻿using System.Collections.Generic;

using UrbanStreetz.Models.IdentityModels;

namespace UrbanStreetz.Models.AppModels {
    public class Highlight : BaseModel {
        public string Name { get; set; }
        public int CreatorUserId { get; set; }
        public User CreatorUser { get; set; }
        public IEnumerable<Story> Stories { get; set; }
    }
}
