using UrbanStreetz.Models.IdentityModels;

namespace UrbanStreetz.Models.AppModels
{
    public class Like : BaseModel
    {
        public int UserId { get; set; }
        public int? PostId { get; set; }
        public int? CommentId { get; set; }
        public Post Post { get; set; }
        public Comment Comment { get; set; }
        public User User { get; set; }
        public bool IsLike { get; set; }
        public bool IsDislike { get; set; }
    }
}