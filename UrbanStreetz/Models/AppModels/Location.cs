﻿using System;
namespace UrbanStreetz.Models.AppModels
{
    public class Location :BaseModel
    {
        public Location()
        {
        }
        public int? UserId { get; set; }
        public int? LocationTypeId { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public string Address { get; set; }
        public int CountryId { get; set; }
        public int? StateId { get; set; }
        public int? CityId { get; set; }
        public string Street { get; set; }
        public virtual LocationType LocationType { get; set; }
        public virtual Country Country { get; set; }
        public virtual State State { get; set; }
        public virtual City City { get; set; }
    }
}

