﻿using System;
namespace UrbanStreetz.Models.AppModels
{
    public class LocationType : BaseModel
    {
        public LocationType()
        {
        }
           public string Name { get; set; }
    }

    public enum LocationTypes
    {
        HOME = 1,
        WORK,
        OTHERS
    }
}

