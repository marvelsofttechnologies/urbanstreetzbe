﻿using System;
using System.ComponentModel.DataAnnotations;
using UrbanStreetz.Models.IdentityModels;

namespace UrbanStreetz.Models.AppModels
{
    public class Media : BaseModel
    {
        public Media()
        {
        }
        [Required]
        [MaxLength(60)]
        public string Name { get; set; }
        [Required]
        [MaxLength(60)]
        public string Extention { get; set; }
        [Required]
        [MaxLength(1000)]
        public string Url { get; set; }
        public bool IsImage { get; set; }
        public bool IsVideo { get; set; }
        public bool IsDocument { get; set; }
        public int? UserId { get; set; }
        public int? PostId { get; set; }
        public int? MessageId { get; set; }
        public string Base64String { get; set; }
        public virtual User User { get; set; }
        public virtual Post Post { get; set; }
    }
}


