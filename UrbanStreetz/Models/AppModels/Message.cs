﻿using UrbanStreetz.Models.IdentityModels;

namespace UrbanStreetz.Models.AppModels {
    public class Message : BaseModel {
        public int SenderId { get; set; }
        public int? RecipientId { get; set; }
        public int? GroupId { get; set; }
        public string Text { get; set; }
        public int? ParentId { get; set; }
        public bool IsDeleted { get; set; } = false;
        public Message Parent { get; set; }
        public User Sender { get; set; }
        public User Recipient { get; set; }
        public int? MediaId { get; set; }
        public Media Media { get; set; }
    }
}
