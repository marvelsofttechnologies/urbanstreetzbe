﻿using System.Collections;
using System.Collections.Generic;
using UrbanStreetz.Models.IdentityModels;

namespace UrbanStreetz.Models.AppModels
{
    public class Post : BaseModel
    {
        public string Feed { get; set; }
        public int UserId {get;set;}
        public User User { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
        public int LocationId { get; set; }
        public Location Loaction { get; set; }
        public int? PostCategoryId { get; set; }
        public PostCategory PostCategory { get; set; }
        public string Brand { get; set; }
        public string Size { get; set; }
        public int Quantity { get; set; }
        public string Delivery { get; set; }
        public string PaymentUrl { get; set; }
        public int? StatusId { get; set; }
        public Status Status { get; set; }
        public int NumberOfLikes {get;set;}
        public int NumberOfDislikes { get; set; }
        public IEnumerable<Media> Medias { get; set; }
        public IEnumerable<Comment> Comments { get; set; }
        public IEnumerable<Like> Likes { get; set; }
    }
}