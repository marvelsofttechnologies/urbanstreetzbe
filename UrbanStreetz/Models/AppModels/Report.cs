﻿using System;
namespace UrbanStreetz.Models.AppModels
{
    public class Report : BaseModel
    {
        public Report()
        {
        }
        public string Message { get; set; }
        public int? CommentId { get; set; }
        public int? PostId { get; set; }
        public Comment Comment { get; set; }
        public Post Post { get; set; }

    }
}

