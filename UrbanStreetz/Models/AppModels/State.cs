﻿using System;
using System.Collections.Generic;

namespace UrbanStreetz.Models.AppModels
{
    public class State 
    {
        public State()
        {
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public int? CountryId { get; set; }
        public Country Country { get; set; }
        public IEnumerable<City> Cities { get; set; }
    }
}

