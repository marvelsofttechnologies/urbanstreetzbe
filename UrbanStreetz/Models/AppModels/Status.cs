﻿namespace UrbanStreetz.Models.AppModels
{
    public class Status : BaseModel
    {
        public string Name { get; set; }
    }

    public enum Statuses
    {
        PENDING = 1,
    }
}