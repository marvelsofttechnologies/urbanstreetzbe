using System;
using UrbanStreetz.Models.IdentityModels;

namespace UrbanStreetz.Models.AppModels
{
    public class Story : BaseModel
    {
        public int UserId { get; set; }
        public User User { get; set; }
        public string Description { get; set; }
        public int? MediaId { get; set; }
        public Media Media { get; set; } 
        public DateTime ExpiryDate { get; set; }
        public int? HighlightId { get; set; }
        public Highlight Highlight { get; set; }
    }
}