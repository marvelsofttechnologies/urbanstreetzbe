﻿using System.Collections.Generic;

namespace UrbanStreetz.Models.AppModels
{

    public class StoryGroup
    {
        public int UserId { get; set; }
        public IEnumerable<Story> Story { get; set; }
    }
}
