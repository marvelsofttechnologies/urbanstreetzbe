using UrbanStreetz.Models.IdentityModels;

namespace UrbanStreetz.Models.AppModels
{
    public class UserInterest : BaseModel
    {
        public int? UserId { get; set; }
        public User User { get; set; }
        public int? PostCategoryId { get; set; }
        public PostCategory PostCategory { get; set; }
    }
}