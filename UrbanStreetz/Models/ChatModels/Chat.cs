using System.Collections.Generic;
using UrbanStreetz.Models.AppModels;
using UrbanStreetz.Models.IdentityModels;

namespace UrbanStreetz.Models.ChatModels
{
    public class Chat : BaseModel
    {
        public string Name { get; set; }
        public ICollection<Message> Messages { get; set; }
        public ICollection<ChatUser> Users { get; set; }
        public ChatType ChatType { get; set; }
    }

    public enum ChatType {
        Private,
        Room
    }
}