using System;
using UrbanStreetz.Models.AppModels;
using UrbanStreetz.Models.IdentityModels;

namespace UrbanStreetz.Models.ChatModels
{
    public class ChatUser 
    {
        public int UserId { get; set; }
        public User User { get; set; }
        public int ChatId { get; set; }
        public Chat Chat { get; set; }
        public ChatUserRole Role { get; set; }
        public DateTime DateCreated { get; set; }
    }

    public enum ChatUserRole
    {
        Admin,
        Member,
    }
}