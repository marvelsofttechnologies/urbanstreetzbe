﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;
using UrbanStreetz.Models.AppModels;
using UrbanStreetz.Models.ChatModels;

namespace UrbanStreetz.Models.IdentityModels
{
    public class User : IdentityUser<int>
    {
        public User()
        {
        }

        [Required]
        [MaxLength(60)]
        public string FirstName { get; set; }
        [Required]
        [MaxLength(60)]
        public string LastName { get; set; }
        [NotMapped]
        public string FullName { get {return $"{FirstName} {LastName}";} }
        public DateTime DateOfBirth { get; set; }
        public int GenderId { get; set; }
        [NotMapped]
        public string Password { get; set; }
        public int? ProfilePictureId { get; set; }
        public Media ProfilePicture { get; set; }
        public IEnumerable<Location> Locations { get; set; }
        public IEnumerable<PostCategory> UserInterests { get; set; }
        public IEnumerable<Post> Posts { get; set; }
        public IEnumerable<UserFollowers> Followers { get; set; }
        public IEnumerable<UserFollowers> Following { get; set; }
        public IEnumerable<Story> Stories { get; set; }
        public IEnumerable<ChatUser> Chats { get; set; }
        public string Bio1 { get; set; }
        public string Bio2 { get; set; }
        public string Bio3 { get; set; }
        public string WebAddress { get; set; }
        [NotMapped]
        public string Token { get; set; }
    }
}

