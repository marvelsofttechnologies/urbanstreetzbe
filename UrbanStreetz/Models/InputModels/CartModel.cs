﻿namespace UrbanStreetz.Models.InputModels {
    public class CartModel {
        public int PostId { get; set; }
        public int Quantity { get; set; }
    }
}
