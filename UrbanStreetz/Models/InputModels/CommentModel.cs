namespace UrbanStreetz.Models.InputModels
{
    public class CommentModel
    {
        public int PostId { get; set; }
        public string CommentText { get; set; }
        public int CommentId { get; set; }
    }
}