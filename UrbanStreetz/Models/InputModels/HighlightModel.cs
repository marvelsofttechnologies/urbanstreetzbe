﻿namespace UrbanStreetz.Models.InputModels {
    public class HighlightModel {
        public int Id { get; set; }
        public string Name { get; set; }
        public int StoryId { get; set; }
    }
}
