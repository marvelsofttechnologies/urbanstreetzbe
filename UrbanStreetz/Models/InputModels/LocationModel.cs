namespace UrbanStreetz.Models.InputModels
{
    public class LocationModel
    {
        public int CityId { get; set; }
        public int CountryId { get; set; }
        public int StateId { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}