namespace UrbanStreetz.Models.InputModels
{
    public class LoginModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}