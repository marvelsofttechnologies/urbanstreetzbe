﻿namespace UrbanStreetz.Models.InputModels
{
    public class MessageModel
    {
        public int? RecipientId { get; set; }
        public int? GroupId { get; set; }
        public string Text { get; set; }
        public int? ParentId { get; set; }
        public MediaModel Media { get; set; }
    }
}
