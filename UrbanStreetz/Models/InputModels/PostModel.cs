namespace UrbanStreetz.Models.InputModels
{
    public class PostModel
    {
        public string Feed { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
        public int LocationId { get; set; }
        public int? PostCategoryId { get; set; }
        public string Brand { get; set; }
        public string Size { get; set; }
        public int Quantity { get; set; }
        public string Delivery { get; set; }
        public string PaymentUrl { get; set; }
        public MediaModel[] Medias { get; set; }
    }
}