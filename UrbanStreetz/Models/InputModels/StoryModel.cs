using System;

namespace UrbanStreetz.Models.InputModels
{
    public class StoryModel
    {
        public string Description { get; set; }
        public MediaModel Media { get; set; }
        //public DateTime ExpiryDate { get; set; }
    }
}