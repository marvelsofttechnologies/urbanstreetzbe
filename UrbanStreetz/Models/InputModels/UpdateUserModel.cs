namespace UrbanStreetz.Models.InputModels
{
    public class UpdateUserModel
    {
        public int Id { get; set; }
        public string PhoneNumber { get; set; }
        public MediaModel ProfilePicture { get; set; }
        public string Bio1 { get; set; }
        public string Bio2 { get; set; }
        public string Bio3 { get; set; }
        public string WebAddress { get; set; }
    }
}