﻿using System;
namespace UrbanStreetz.Models.InputModels
{
    public class UserModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public LocationModel[] Locations { get; set; }
        public int[] Categories { get; set; }
    }
}

