namespace UrbanStreetz.Models.ViewModels
{
    public class BaseNameModelView
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}