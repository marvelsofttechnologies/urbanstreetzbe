﻿using System;

namespace UrbanStreetz.Models.ViewModels {
    public class CartView {
        public int Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public int PostId { get; set; }
        public string PostFeed { get; set; }
        public string PostDescription { get; set; }
        public string PostBrand { get; set; }
        public int Quantity { get; set; }
        public string PostSize { get; set; }
    }
}
