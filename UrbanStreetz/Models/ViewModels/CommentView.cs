using System.Collections.Generic;

namespace UrbanStreetz.Models.ViewModels {
    public class CommentView {
        public UserView User { get; set; }
        public string Status { get; set; }
        public string CommentText { get; set; }
        public IEnumerable<CommentView> Comments { get; set; }
    }
}