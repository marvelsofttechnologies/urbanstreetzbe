namespace UrbanStreetz.Models.ViewModels
{
    public class CountryView
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        // public StateView[] States { get; set; } 
    }
}