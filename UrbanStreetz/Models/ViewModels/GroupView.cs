﻿namespace UrbanStreetz.Models.ViewModels
{
    public class GroupView
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string CreatorName { get; set; }
    }
}
