﻿using System.Collections.Generic;

namespace UrbanStreetz.Models.ViewModels {
    public class HighlightView {
        public int Id { get; set; }
        public string Name { get; set; }
        public string CreatorUser { get; set; }
        public IEnumerable<StoryView> Stories { get; set; }
    }
}
