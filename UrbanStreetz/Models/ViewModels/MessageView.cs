﻿namespace UrbanStreetz.Models.ViewModels {
    public class MessageView {
        public int SenderId { get; set; }
        public int? RecipientId { get; set; }
        public int? GroupId { get; set; }
        public string Text { get; set; }
        public int? ParentId { get; set; }
        public string SenderName { get; set; }
        public string RecipientName { get; set; }
        public string ParentText { get; set; }
        public string GroupName { get; set; }
    }
}
