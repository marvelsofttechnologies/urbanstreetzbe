using System;
namespace UrbanStreetz.Models.ViewModels
{
    public class PostCategoryView 
    {
        public int Id { get; set;}
        public string Name { get; set; }
    }
}

