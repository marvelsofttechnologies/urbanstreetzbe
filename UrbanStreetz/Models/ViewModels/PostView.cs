using System;
using System.Collections.Generic;

namespace UrbanStreetz.Models.ViewModels
{
    public class PostView
    {
        public int Id { get; set; }
        public string Feed { get; set; }
        public DateTime DateCreated { get; set; }
        public UserView User { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
        public int LocationId { get; set; }
        public string PostCategory { get; set; }
        public string Brand { get; set; }
        public string Size { get; set; }
        public int Quantity { get; set; }
        public string Delivery { get; set; }
        public string PaymentUrl { get; set; }
        public string Status { get; set; }
        public IEnumerable<MediaView> Medias { get; set; }
        public int NumberOfLikes { get; set; }

    }
}