﻿using System.Collections.Generic;

namespace UrbanStreetz.Models.ViewModels
{
    public class StoryGroupView
    {
        public int UserId { get; set; }
        public IEnumerable<StoryView> Story { get; set; }
    }

}
