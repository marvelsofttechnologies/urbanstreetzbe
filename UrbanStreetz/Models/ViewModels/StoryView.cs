using System;
using System.Collections.Generic;

namespace UrbanStreetz.Models.ViewModels
{
    public class StoryView
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string FullName { get; set; }
        public string Description { get; set; }
        public int? MediaId { get; set; }
        public string MediaUrl { get; set; }
        public DateTime ExpiryDate { get; set; }
    }
}