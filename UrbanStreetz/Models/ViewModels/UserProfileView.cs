﻿using System.Collections.Generic;

namespace UrbanStreetz.Models.ViewModels {
    public class UserProfileView {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string Bio1 { get; set; }
        public string Bio2 { get; set; }
        public string Bio3 { get; set; }
        public string WebAddress { get; set; }
        public string Gender { get; set; }
        public string ProfilePictureUrl { get; set; }
        public int NoOfFollowers { get; set; }
        public int NoFollowing { get; set; }
        public int NoOfPosts { get; set; }
        public IEnumerable<StoryView> Stories { get; set; }
    }
}
