namespace UrbanStreetz.Models.ViewModels
{
    public class UserView
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public string Token { get ; set;}
    }
}