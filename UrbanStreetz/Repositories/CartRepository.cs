﻿using System;
using System.Collections.Generic;

using Microsoft.EntityFrameworkCore;

using UrbanStreetz.Context;
using UrbanStreetz.Models.AppModels;
using UrbanStreetz.Repositories.Interfaces;
using UrbanStreetz.Utilities;

namespace UrbanStreetz.Repositories {
    public class CartRepository : BaseRepository<Cart>, ICartRepository {
        public CartRepository(AppDbContext context) : base(context) { }

        public new Cart CreateAndReturn(Cart model) {
            try {
                var result = _context.Add(model);

                return result.Entity;
            }
            catch (Exception ex) {
                Logger.Error(ex);

                return null;
            }
        }

        public IEnumerable<Cart> ListCart() {
            try {
                var result = _context.Carts
                                     .Include(cart => cart.CreatorUser)
                                     .Include(cart => cart.Post);

                return result;
            }
            catch (Exception ex) {
                Logger.Error(ex);

                return null;
            }
        }
    }
}
