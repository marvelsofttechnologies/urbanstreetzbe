using UrbanStreetz.Context;
using UrbanStreetz.Models.ChatModels;
using UrbanStreetz.Repositories.Interfaces;

namespace UrbanStreetz.Repositories
{
    public class ChatRepository : BaseRepository<Chat>, IChatRepository
    {
        public ChatRepository(AppDbContext context) : base(context)
        {
        }

        public ChatUser CreateChatUser(ChatUser user)
        {
            _context.ChatUsers.Add(user);
            _context.SaveChanges();
            return user;
        }
    }
}