using System;
using System.Collections.Generic;
using UrbanStreetz.Context;
using UrbanStreetz.Models.AppModels;
using UrbanStreetz.Repositories.Interfaces;
using UrbanStreetz.Utilities;

namespace UrbanStreetz.Repositories
{
    public class CodeRepository : BaseRepository<Code>, ICodeRepository
    {
        public CodeRepository(AppDbContext context) : base(context) { }

        public IEnumerable<Code> List()
        {
            try
            {
                var result = _context.Codes;

                return result;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return null;
            }
        }
    }
}