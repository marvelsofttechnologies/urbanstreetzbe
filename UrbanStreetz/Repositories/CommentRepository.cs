using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using UrbanStreetz.Context;
using UrbanStreetz.Models.AppModels;
using UrbanStreetz.Repositories.Interfaces;

namespace UrbanStreetz.Repositories
{
    public class CommentRepository : BaseRepository<Comment>, ICommentRepository
    {
        public CommentRepository(AppDbContext context) : base(context)
        {
        }

        public IEnumerable<Comment> List()
        {
            return _context.Comments
                .Where(c => !c.CommentId.HasValue)
                .Include(c => c.User)
                .Include(c => c.Post)
                .Include(c => c.Comments).Take(50);
        }
    }
}