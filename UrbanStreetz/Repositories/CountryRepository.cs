using System;
using System.Collections.Generic;
using System.Linq;
using UrbanStreetz.Context;
using UrbanStreetz.Models.AppModels;
using UrbanStreetz.Repositories.Interfaces;
using UrbanStreetz.Utilities;

namespace UrbanStreetz.Repositories
{
    public class CountryRepository : ICountryRepository
    {
        private readonly AppDbContext _context;
        public CountryRepository(AppDbContext context)
        {
            _context = context;
        }

        public IEnumerable<City> ListCities(int stateId)
        {
            try
            {
                 return _context.Cities.Where(c => c.StateId == stateId);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return null;
            }
        }

        public IEnumerable<Country> ListCountries()
        {
            try
            {
                return _context.Set<Country>();
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return null;
            }
        }

        public IEnumerable<State> ListStates(int countryId)
        {
            try
            {
                 return _context.States.Where(c => c.CountryId == countryId);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return null;
            }
        }
    }
}