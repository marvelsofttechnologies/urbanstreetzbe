﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using UrbanStreetz.Context;
using UrbanStreetz.Models.AppModels;
using UrbanStreetz.Repositories.Interfaces;
using UrbanStreetz.Utilities;

namespace UrbanStreetz.Repositories
{
    public class GroupRepository : BaseRepository<Group>, IGroupRepository
    {
        public GroupRepository(AppDbContext context) : base(context) { }

        public GroupUser CreateGroupUser(GroupUser model) {
            try {
                _context.GroupUsers.Add(model);
                _context.SaveChanges();
                
                return model;
            }
            catch (Exception ex) {
                Logger.Error(ex);
                return null;
            }
        }

        public IEnumerable<Group> ListUserGroup(int userId)
        {
            try
            {
                var groups = _context.Groups
                    .Where(group => !group.IsDeleted
                        && group.Users.Any(user => user.UserId == userId))
                    .OrderByDescending(group => group.DateModified);

                return groups;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return null;
            }
        }

        public IEnumerable<Group> AllGroups()
        {
            try
            {
                var groups = _context.Groups
                    .Where(group => !group.IsDeleted)
                    .OrderByDescending(group => group.DateModified)
                    .Include(group => group.CreatorUser)
                    .Include(group => group.Users);

                return groups;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return null;
            }
        }
    }
}
