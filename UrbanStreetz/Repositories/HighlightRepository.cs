using Microsoft.EntityFrameworkCore;

using System;
using System.Collections.Generic;
using System.Linq;

using UrbanStreetz.Context;
using UrbanStreetz.Models.AppModels;
using UrbanStreetz.Repositories.Interfaces;
using UrbanStreetz.Utilities;

namespace UrbanStreetz.Repositories {
    public class HighlightRepository : BaseRepository<Highlight>, IHighlightRepository {
        public HighlightRepository(AppDbContext context) : base(context) { }

        public IEnumerable<Highlight> ListHighlights(int userId) {
            try {
                var userHighlights = _context.Highlights.Where(highlight => highlight.CreatorUserId == userId)
                                             .Include(highlight => highlight.CreatorUser)
                                             .Include(highlight => highlight.Stories);

                return userHighlights;
            }
            catch (Exception ex) {
                Logger.Error(ex);

                return null;
            }
        }

        public bool RemoveAllStories(int highlightId) {
            try {
                var stories = _context.Stories.Where(story => story.HighlightId == highlightId);

                foreach (var story in stories) {
                    story.HighlightId = null;
                }

                _context.SaveChanges();

                return true;
            }
            catch (Exception ex) {
                Logger.Error(ex);

                return false;
            }
        }
    }
}
