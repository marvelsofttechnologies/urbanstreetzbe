﻿using System.Collections.Generic;
using UrbanStreetz.Models.AppModels;

namespace UrbanStreetz.Repositories.Interfaces {
    public interface ICartRepository : IBaseRepository<Cart> {
        IEnumerable<Cart> ListCart( );
    }
}
