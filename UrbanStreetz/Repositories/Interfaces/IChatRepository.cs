using System.Collections;
using System.Collections.Generic;
using UrbanStreetz.Models.ChatModels;

namespace UrbanStreetz.Repositories.Interfaces
{
    public interface IChatRepository
    {
         Chat CreateAndReturn(Chat chat);
         ChatUser CreateChatUser(ChatUser user);
         IEnumerable<Chat> GetAll();
    }
}