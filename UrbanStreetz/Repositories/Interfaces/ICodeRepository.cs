using UrbanStreetz.Models.AppModels;

namespace UrbanStreetz.Repositories.Interfaces
{
    public interface ICodeRepository 
    {
        Code GetById(int id);
         void Delete(Code Code);
    }
}