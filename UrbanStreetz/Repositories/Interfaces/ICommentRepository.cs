using System.Collections;
using System.Collections.Generic;
using UrbanStreetz.Models.AppModels;

namespace UrbanStreetz.Repositories.Interfaces
{
    public interface ICommentRepository
    {
         Comment CreateAndReturn(Comment comment);
         Comment Update(Comment comment);
         void Delete(Comment comment);
         IEnumerable<Comment> List();
    }
}