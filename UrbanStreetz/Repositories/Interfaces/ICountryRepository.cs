using System.Collections;
using System.Collections.Generic;
using UrbanStreetz.Models.AppModels;

namespace UrbanStreetz.Repositories.Interfaces
{
    public interface ICountryRepository
    {
        IEnumerable<Country> ListCountries();
        IEnumerable<State> ListStates(int countryId);
        IEnumerable<City> ListCities(int stateId);
    }
}