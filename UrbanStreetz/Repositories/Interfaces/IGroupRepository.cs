﻿using System.Collections.Generic;
using UrbanStreetz.Models.AppModels;

namespace UrbanStreetz.Repositories.Interfaces
{
    public interface IGroupRepository : IBaseRepository<Group>
    {
        IEnumerable<Group> ListUserGroup(int userId);
        IEnumerable<Group> AllGroups();
        GroupUser CreateGroupUser(GroupUser model);
    }
}
