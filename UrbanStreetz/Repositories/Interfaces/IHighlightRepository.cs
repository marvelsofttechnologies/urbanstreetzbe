using System.Collections.Generic;
using UrbanStreetz.Models.AppModels;

namespace UrbanStreetz.Repositories.Interfaces
{
    public interface IHighlightRepository : IBaseRepository<Highlight>
    {
        IEnumerable<Highlight> ListHighlights(int userId);
        bool RemoveAllStories(int highlightId);
    }
}