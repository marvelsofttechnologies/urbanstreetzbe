using System.Collections.Generic;
using UrbanStreetz.Models.AppModels;

namespace UrbanStreetz.Repositories.Interfaces
{
    public interface ILikeRepository : IBaseRepository<Like>
    {
         Like CreateAndReturn(Like like);

         IEnumerable<Like> ListLikes();
    }
}