using UrbanStreetz.Models.AppModels;

namespace UrbanStreetz.Repositories.Abstractions
{
    public interface ILocationRepository
    {
        Location CreateAndReturn(Location model);
    }
}