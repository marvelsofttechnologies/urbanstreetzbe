using System.Collections.Generic;
using System.Threading.Tasks;
using UrbanStreetz.Models.AppModels;

namespace UrbanStreetz.Repositories.Interfaces
{
    public interface IMediaRepository 
    {
         Task<(bool Succeeded, Media UploadedMedia)> UploadMedia(Media media);
         List<Media> UploadBulkMedia(List<Media> medias);
         List<Media> GetByPropertyId(int id);
         Media GetById(int id);
         void Delete(Media media);
    }
}