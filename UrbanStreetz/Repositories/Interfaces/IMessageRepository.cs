﻿using System.Collections.Generic;
using UrbanStreetz.Models.AppModels;

namespace UrbanStreetz.Repositories.Interfaces
{
    public interface IMessageRepository : IBaseRepository<Message>
    {
        IEnumerable<Message> ListMessages();
        IEnumerable<Message> ChatMessages(int userId, int recipientId);
        IEnumerable<Message> GroupMessages(int groupId);
        new Message CreateAndReturn(Message model);
        void Delete(int id);
    }
}
