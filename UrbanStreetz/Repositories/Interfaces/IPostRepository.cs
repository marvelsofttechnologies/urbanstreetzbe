using System.Collections;
using System.Collections.Generic;
using UrbanStreetz.Models.AppModels;

namespace UrbanStreetz.Repositories.Interfaces
{
    public interface IPostRepository
    {
         Post CreateAndReturn(Post post);
         IEnumerable<Post> ListPosts();
         IEnumerable<PostCategory> ListPostCategories();
         Post Update(Post post);
         Post GetById(int id);
    }
}