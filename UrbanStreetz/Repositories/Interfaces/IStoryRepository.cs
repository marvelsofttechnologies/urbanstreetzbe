using System.Collections.Generic;
using UrbanStreetz.Models.AppModels;

namespace UrbanStreetz.Repositories.Interfaces
{
    public interface IStoryRepository : IBaseRepository<Story>
    {
        IEnumerable<Story> ListStories();
        void DeleteRange(IEnumerable<Story> stories);
    }
}