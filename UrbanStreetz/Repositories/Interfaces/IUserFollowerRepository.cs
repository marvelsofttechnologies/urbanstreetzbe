using System.Collections.Generic;
using UrbanStreetz.Models.AppModels;

namespace UrbanStreetz.Repositories.Interfaces
{
    public interface IUserFollowerRepository
    {
         UserFollowers CreateAndReturn(UserFollowers userFollowers);
        UserFollowers GetById(int id);
        IEnumerable<UserFollowers> GetMyFollowings(int userId);
        IEnumerable<UserFollowers> GetUserFollowers(int userId);
        void Delete(UserFollowers userFollowers);
    }
}