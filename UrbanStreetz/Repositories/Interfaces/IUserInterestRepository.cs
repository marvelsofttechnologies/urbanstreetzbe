using UrbanStreetz.Models.AppModels;

namespace UrbanStreetz.Repositories.Interfaces
{
    public interface IUserInterestRepository: IBaseRepository<UserInterest>
    {
         UserInterest CreateAndReturn(UserInterest model);
    }
}