using System;
using System.Collections.Generic;

using Microsoft.EntityFrameworkCore;

using UrbanStreetz.Context;
using UrbanStreetz.Models.AppModels;
using UrbanStreetz.Repositories.Interfaces;
using UrbanStreetz.Utilities;

namespace UrbanStreetz.Repositories
{
    public class LikeRepository : BaseRepository<Like>, ILikeRepository
    {
        public LikeRepository(AppDbContext context) : base(context)
        {
        }

        public IEnumerable<Like> ListLikes() {
            try {
                var likes = _context.Likes
                                    .Include(like => like.User)
                                    .Include(like => like.Post);

                return likes;
            }
            catch (Exception ex) {
                Logger.Error(ex);

                return null;
            }
        }
    }
}