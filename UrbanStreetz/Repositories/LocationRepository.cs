using UrbanStreetz.Context;
using UrbanStreetz.Models.AppModels;
using UrbanStreetz.Repositories.Abstractions;

namespace UrbanStreetz.Repositories
{
    public class LocationRepository : BaseRepository<Location>, ILocationRepository
    {
        public LocationRepository(AppDbContext context) : base(context)
        {
        }
    }
}