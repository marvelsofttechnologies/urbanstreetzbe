using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using UrbanStreetz.ContentServer;
using UrbanStreetz.Context;
using UrbanStreetz.Models.AppModels;
using UrbanStreetz.Repositories.Interfaces;
using UrbanStreetz.Utilities;
using UrbanStreetz.Utilities.Abstrctions;
using static UrbanStreetz.ContentServer.BaseContentServer;

namespace UrbanStreetz.Repositories
{
    public class MediaRepository : BaseRepository<Media>, IMediaRepository
    {
        private readonly Globals _globals;
        private readonly IUtilityMethods _utilityMethods;
        public MediaRepository(AppDbContext context, IOptions<Globals> globals, IUtilityMethods utilityMethods) : base(context)
        {
            _globals = globals.Value;
            _utilityMethods = utilityMethods;
        }

        public List<Media> GetByPropertyId(int id)
        {
            throw new System.NotImplementedException();
        }

        public List<Media> UploadBulkMedia(List<Media> medias)
        {
            throw new System.NotImplementedException();
        }

        public async Task<(bool Succeeded, Media UploadedMedia)> UploadMedia(Media media)
        {
            FileDocument UploadResult = FileDocument.Create();
            try
            {
                UploadResult = await BaseContentServer
                .Build(ContentServerTypeEnum.GOOGLEDRIVE, _globals, _utilityMethods)
                .UploadDocumentAsync(FileDocument.Create(media.Base64String, media.Name, "", FileDocumentType.GetDocumentType(media.Extention)));

                media.Url = UploadResult.Path;

                var CreatedMedia = CreateAndReturn(media);
                return (true, CreatedMedia);
            }
            catch (Exception e)
            {
                Logger.Error(e);
                return (false, null);
            }
        }
    }
}