﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using UrbanStreetz.Context;
using UrbanStreetz.Models.AppModels;
using UrbanStreetz.Repositories.Interfaces;
using UrbanStreetz.Utilities;

namespace UrbanStreetz.Repositories
{
    public class MessageRepository : BaseRepository<Message>, IMessageRepository
    {
        public MessageRepository(AppDbContext context) : base(context) { }

        public IEnumerable<Message> ListMessages()
        {
            try
            {
                var messages = _context.Messages
                    .Where(message => !message.IsDeleted)
                    .Include(message => message.Parent)
                    .Include(message => message.Sender)
                    .OrderByDescending(message => message.DateCreated);

                return messages;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return null;
            }
        }
        
        public IEnumerable<Message> ChatMessages(int userId, int recipientId)
        {
            try
            {
                var messages = _context.Messages
                    .Where(message => !message.IsDeleted
                        && message.SenderId == userId
                        && message.RecipientId == recipientId)
                    .Include(message => message.Parent)
                    .Include(message => message.Sender)
                    .OrderByDescending(message => message.DateCreated);

                return messages;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return null;
            }
        }

        public IEnumerable<Message> GroupMessages(int groupId)
        {
            try
            {
                var messages = _context.Messages
                    .Where(message => !message.IsDeleted
                        && message.GroupId == groupId)
                    .Include(message => message.Parent)
                    .Include(message => message.Sender)
                    .OrderByDescending(message => message.DateCreated);

                return messages;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return null;
            }
        }

        public void Delete(int id)
        {
            try
            {
                var message = _context.Messages.Find(id);

                if (message != null)
                    _context.Messages.Remove(message);

                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }

        public new Message CreateAndReturn(Message model)
        {
            try
            {
                _context.Messages.Add(model);
                _context.SaveChanges();

                var savedMessage = _context.Messages
                    .Where(message => message.Id == model.Id)
                    .Include(message => message.Sender)
                    .Include(message => message.Parent)
                    .FirstOrDefault();

                return savedMessage;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return null;
            }
        }
    }
}
