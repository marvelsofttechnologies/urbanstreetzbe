using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using UrbanStreetz.Context;
using UrbanStreetz.Models.AppModels;
using UrbanStreetz.Repositories.Interfaces;
using UrbanStreetz.Utilities;

namespace UrbanStreetz.Repositories
{
    public class PostRepository : BaseRepository<Post>, IPostRepository
    {
        public PostRepository(AppDbContext context) : base(context)
        {
        }

        public IEnumerable<Post> ListPosts()
        {
            try
            {
                var result = _context.Posts.Include(post => post.PostCategory)
                                     .Include(post => post.Loaction)
                                     .Include(post => post.Comments).Take(50)
                                     .Include(post => post.Medias)
                                     .Include(post => post.User);

                return result;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return null;
            }
        }

        public IEnumerable<PostCategory> ListPostCategories()
        {
            try
            {
                return _context.PostCategories;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return null;
            }
        }
    }
}