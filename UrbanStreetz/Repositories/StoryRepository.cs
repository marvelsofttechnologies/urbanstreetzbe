using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using UrbanStreetz.Context;
using UrbanStreetz.Models.AppModels;
using UrbanStreetz.Repositories.Interfaces;
using UrbanStreetz.Utilities;

namespace UrbanStreetz.Repositories
{
    public class StoryRepository : BaseRepository<Story>, IStoryRepository
    {
        public StoryRepository(AppDbContext context) : base(context)
        {
        }

        public IEnumerable<Story> ListStories()
        {
            try
            {
                var userStories = _context.Stories
                     .Include(story => story.User)
                     .Include(story => story.Media);

                return userStories;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return null;
            }
        }

        public void DeleteRange(IEnumerable<Story> stories)
        {
            try
            {
                _context.Stories.RemoveRange(stories);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }
    }
}