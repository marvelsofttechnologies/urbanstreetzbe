using System.Collections.Generic;
using System.Linq;

using Microsoft.EntityFrameworkCore;

using UrbanStreetz.Context;
using UrbanStreetz.Models.AppModels;
using UrbanStreetz.Repositories.Interfaces;

namespace UrbanStreetz.Repositories
{
    public class UserFollowerRepository : BaseRepository<UserFollowers>, IUserFollowerRepository
    {
        public UserFollowerRepository(AppDbContext context) : base(context)
        {
        }

        public IEnumerable<UserFollowers> GetUserFollowers(int userId)
        {
            return _context.UserFollowers.Where(x => x.UserId == userId).Include(f => f.User);
        }

        public IEnumerable<UserFollowers> GetMyFollowings(int userId)
        {
            return _context.UserFollowers.Where(x => x.FollowerId == userId).Include(f => f.Follower);
        }
    }
}