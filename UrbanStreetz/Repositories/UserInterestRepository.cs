using UrbanStreetz.Context;
using UrbanStreetz.Models.AppModels;
using UrbanStreetz.Repositories.Interfaces;

namespace UrbanStreetz.Repositories
{
    public class UserInterestRepository : BaseRepository<UserInterest>, IUserInterestRepository
    {
        public UserInterestRepository(AppDbContext context) : base(context)
        {
        }

    }
}