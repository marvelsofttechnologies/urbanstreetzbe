using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Localization;
using UrbanStreetz.ChatServer.Hubs;
using UrbanStreetz.Models.ChatModels;
using UrbanStreetz.Repositories.Abstractions;
using UrbanStreetz.Repositories.Interfaces;
using UrbanStreetz.Services.Interfaces;
using UrbanStreetz.Utilities;
using UrbanStreetz.Utilities.Extentions;

namespace UrbanStreetz.Services
{
    public class ChatService : IChatService
    {
        private readonly IHubContext<ChatHub> _chatHub;
        private readonly IChatRepository _chatRepository;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IGroupRepository _groupRepository;
        public ChatService(IHubContext<ChatHub> chatHub, IChatRepository chatRepository, IHttpContextAccessor httpContextAccessor, IGroupRepository groupRepository)
        {
            _chatHub = chatHub;
            _chatRepository = chatRepository;
            _httpContextAccessor = httpContextAccessor;
            _groupRepository = groupRepository;
        }

        public async void JoinChat(string ConnectionId, string GroupName, int chatId)
        {
            var LoggedInUser = _httpContextAccessor.HttpContext.User.GetLoggedInUserId<int>();
            Chat ExistingChat = _chatRepository.GetAll().FirstOrDefault(x => x.Name == GroupName && x.Users.Any(u => u.UserId == LoggedInUser));
            if (ExistingChat != null) return;
            Chat NewChat = new()
            {
                ChatType = ChatType.Room,
                Name = GroupName,
            };
            NewChat = _chatRepository.CreateAndReturn(NewChat);
            ChatUser thisChatUser = new()
            {
                ChatId = NewChat.Id,
                UserId = LoggedInUser,
                Role = ChatUserRole.Admin
            };

            thisChatUser = _chatRepository.CreateChatUser(thisChatUser);
            await _chatHub.Groups.AddToGroupAsync(ConnectionId, GroupName);
        }
    }
}