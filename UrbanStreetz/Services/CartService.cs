﻿using System;
using System.Linq;

using AutoMapper;
using AutoMapper.QueryableExtensions;

using Microsoft.AspNetCore.Http;

using UrbanStreetz.Controllers;
using UrbanStreetz.Models;
using UrbanStreetz.Models.InputModels;
using UrbanStreetz.Models.UtilityModels;
using UrbanStreetz.Models.ViewModels;
using UrbanStreetz.Repositories.Interfaces;
using UrbanStreetz.Services.Interfaces;
using UrbanStreetz.Utilities;
using UrbanStreetz.Utilities.Extentions;

namespace UrbanStreetz.Services {
    public class CartService : ICartService {
        
        private readonly IMapper _mapper;
        private readonly IConfigurationProvider _configuration;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ICartRepository _cartRepository;

        public StandardResponse<CartView> CreateCart(CartModel model) {
            try {
                int loggedInUserId = _httpContextAccessor.HttpContext.User.GetLoggedInUserId<int>();

                var cart = _cartRepository.ListCart()
                                          .FirstOrDefault(c => c.CreatorUserId == loggedInUserId && c.PostId == model.PostId);

                // just update cart quantity if it exist
                if (cart != null) {
                    cart.Quantity = model.Quantity;
                    cart.DateModified = DateTime.Now;
                    _cartRepository.Update(cart);

                    return StandardResponse<CartView>.Ok(_mapper.Map<CartView>(cart));
                }
                    
                cart.CreatorUserId = loggedInUserId;
                var createdCart = _cartRepository.CreateAndReturn(cart);
                return StandardResponse<CartView>.Ok(_mapper.Map<CartView>(createdCart));
            }
            catch (Exception ex) {
                Logger.Error(ex);

                return StandardResponse<CartView>.Failed(ex.Message);
            }
        }

        public StandardResponse<bool> RemoveCart(int id) {
            try {
                int loggedInUserId = _httpContextAccessor.HttpContext.User.GetLoggedInUserId<int>();

                var cart = _cartRepository.ListCart()
                                          .FirstOrDefault(c => c.Id == id && c.CreatorUserId == loggedInUserId);

                if (cart != null) _cartRepository.Delete(cart);

                return StandardResponse<bool>.Ok(true);
            }
            catch (Exception ex) {
                Logger.Error(ex);

                return StandardResponse<bool>.Failed(ex.Message);
            }
        }

        public StandardResponse<PagedCollection<CartView>> ListCart(PagingOptions pagingOptions) {
            try {
                int loggedInUserId = _httpContextAccessor.HttpContext.User.GetLoggedInUserId<int>();

                var carts = _cartRepository.ListCart()
                                           .Where(c => c.CreatorUserId == loggedInUserId)
                                           .AsQueryable()
                                           .ProjectTo<CartView>(_configuration)
                                           .AsEnumerable();

                var pagedCarts = carts.Skip(pagingOptions.Offset.Value).Take(pagingOptions.Limit.Value);
                var pagedResponse = PagedCollection<CartView>.Create(
                    Link.ToCollection(nameof(CartController.ListCart)),
                    pagedCarts.ToArray(),
                    carts.Count(),
                    pagingOptions);

                return StandardResponse<PagedCollection<CartView>>.Ok(pagedResponse);
            }
            catch (Exception ex) {
                Logger.Error(ex);

                return StandardResponse<PagedCollection<CartView>>.Failed(ex.Message);
            }
        }
    }
}
