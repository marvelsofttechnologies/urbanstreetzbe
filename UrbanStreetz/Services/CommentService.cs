using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using AutoMapper.Configuration;
using AutoMapper.QueryableExtensions;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Hosting;
using UrbanStreetz.Controllers;
using UrbanStreetz.Models;
using UrbanStreetz.Models.AppModels;
using UrbanStreetz.Models.InputModels;
using UrbanStreetz.Models.UtilityModels;
using UrbanStreetz.Models.ViewModels;
using UrbanStreetz.Repositories;
using UrbanStreetz.Repositories.Interfaces;
using UrbanStreetz.Services.Interfaces;
using UrbanStreetz.Utilities;
using UrbanStreetz.Utilities.Extentions;

namespace UrbanStreetz.Services
{
    public class CommentService : ICommentService
    {
        private readonly ICommentRepository _commentRepository;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IMapper _mapper;
        private readonly IConfigurationProvider _configuration;
        private readonly ILikeRepository _likeRepository;

        public CommentService(ICommentRepository commentRepository, IHttpContextAccessor httpContextAccessor, IMapper mapper, IConfigurationProvider configuration, ILikeRepository likeRepository)
        {
            _commentRepository = commentRepository;
            _httpContextAccessor = httpContextAccessor;
            _mapper = mapper;
            _configuration = configuration;
            _likeRepository = likeRepository;
        }

        public StandardResponse<CommentView> New(CommentModel comment)
        {
            try
            {
                var LoggedInUser = _httpContextAccessor.HttpContext.User.GetLoggedInUserId<int>();
                var mappedComment = _mapper.Map<Comment>(comment);
                mappedComment.UserId = LoggedInUser;

                if(mappedComment.CommentId == 0)
                    mappedComment.CommentId = null;

                var newComment = _commentRepository.CreateAndReturn(mappedComment);

                return  StandardResponse<CommentView>.Ok(_mapper.Map<CommentView>(newComment));
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return StandardResponse<CommentView>.Failed(ex.Message);
            }
        }

        public StandardResponse<PagedCollection<CommentView>> ListPostComments(int PostId, PagingOptions pagingOptions)
        {
            try
            {
                var comments = _commentRepository.List().Where(comment => comment.PostId == PostId).AsQueryable().ProjectTo<CommentView>(_configuration).AsEnumerable();

                var pagedComments = comments.Skip(pagingOptions.Offset.Value).Take(pagingOptions.Limit.Value);
                var pagedResponse = PagedCollection<CommentView>.Create(
                    Link.ToCollection(nameof(CommentController.ListPostsComments)),
                    pagedComments.ToArray(),
                    comments.Count(),
                    pagingOptions);

                return StandardResponse<PagedCollection<CommentView>>.Ok(pagedResponse);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return StandardResponse<PagedCollection<CommentView>>.Failed(ex.Message);
            }
        }
       
        public StandardResponse<PagedCollection<UserView>> ListCommentLikeUsers(int commentId, PagingOptions pagingOptions) {
            try {
                var postLikedUsers = _likeRepository.ListLikes()
                                                    .Where(like => like.CommentId == commentId && like.IsLike)
                                                    .Select(like => like.User)
                                                    .AsQueryable()
                                                    .ProjectTo<UserView>(_configuration)
                                                    .AsEnumerable();

                var pagedPosts = postLikedUsers.Skip(pagingOptions.Offset.Value).Take(pagingOptions.Limit.Value);
                var pagedResponse = PagedCollection<UserView>.Create(
                    Link.ToCollection(nameof(PostController.ListPopularPosts)),
                    pagedPosts.ToArray(),
                    postLikedUsers.Count(),
                    pagingOptions);

                return StandardResponse<PagedCollection<UserView>>.Ok(pagedResponse);
            }
            catch (Exception ex) {
                Logger.Error(ex);

                return StandardResponse<PagedCollection<UserView>>.Failed(ex.Message);
            }
        }
    }
}