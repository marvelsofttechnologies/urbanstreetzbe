using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.Extensions.Hosting;
using UrbanStreetz.Controllers;
using UrbanStreetz.Models;
using UrbanStreetz.Models.AppModels;
using UrbanStreetz.Models.UtilityModels;
using UrbanStreetz.Models.ViewModels;
using UrbanStreetz.Repositories.Interfaces;
using UrbanStreetz.Services.Interfaces;
using UrbanStreetz.Utilities;

namespace UrbanStreetz.Services
{
    public class CountryService : ICountryService
    {
        private readonly ICountryRepository _countryRepository;
        private readonly IMapper _mapper;
        public readonly IConfigurationProvider _configuration;

        public CountryService(ICountryRepository countryRepository, IMapper mapper, IConfigurationProvider configuration)
        {
            _countryRepository = countryRepository;
            _mapper = mapper;
            _configuration = configuration;
        }

        public StandardResponse<PagedCollection<CountryView>> ListCountries(PagingOptions pagingOptions)
        {
            try
            {
                var countries = _countryRepository.ListCountries().AsQueryable().ProjectTo<CountryView>(_configuration).AsEnumerable();

                var pagedCountries = countries.Skip(pagingOptions.Offset.Value).Take(pagingOptions.Limit.Value);
                var pagedResponse = PagedCollection<CountryView>.Create(
                    Link.ToCollection(nameof(UtilitiesController.ListCountries)),
                    pagedCountries.ToArray(),
                    countries.Count(),
                    pagingOptions);

                return  StandardResponse<PagedCollection<CountryView>>.Ok(pagedResponse);
            }
            catch (Exception ex)
            {
                return  StandardResponse<PagedCollection<CountryView>>.Failed(ex.Message);
            }
        }

        public StandardResponse<PagedCollection<StateView>> ListStates(int countryId, PagingOptions pagingOptions)
        {
            try
            {
                var states = _countryRepository.ListStates(countryId).AsQueryable().ProjectTo<StateView>(_configuration).AsEnumerable();

                var pagedStates = states.Skip(pagingOptions.Offset.Value).Take(pagingOptions.Limit.Value);
                var pagedResponse = PagedCollection<StateView>.Create(
                    Link.ToCollection(nameof(UtilitiesController.ListStates)),
                    pagedStates.ToArray(),
                    states.Count(),
                    pagingOptions);

                return StandardResponse<PagedCollection<StateView>>.Ok(pagedResponse);
            }
            catch (Exception ex)
            {
                return StandardResponse<PagedCollection<StateView>>.Failed(ex.Message);
            }
        }

        public StandardResponse<PagedCollection<BaseNameModelView>> ListCities(int stateId, PagingOptions pagingOptions)
        {
            try
            {
                var cities = _countryRepository.ListCities(stateId).AsQueryable().ProjectTo<BaseNameModelView>(_configuration).AsEnumerable();

                var pagedCities = cities.Skip(pagingOptions.Offset.Value).Take(pagingOptions.Limit.Value);
                var pagedResponse = PagedCollection<BaseNameModelView>.Create(
                    Link.ToCollection(nameof(UtilitiesController.ListCities)),
                    pagedCities.ToArray(),
                    cities.Count(),
                    pagingOptions);

                return StandardResponse<PagedCollection<BaseNameModelView>>.Ok(pagedResponse);
            }
            catch (Exception ex)
            {
                return StandardResponse<PagedCollection<BaseNameModelView>>.Failed(ex.Message);
            }
        }

        
    }
}