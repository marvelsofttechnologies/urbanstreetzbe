﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UrbanStreetz.Controllers;
using UrbanStreetz.Models;
using UrbanStreetz.Models.AppModels;
using UrbanStreetz.Models.InputModels;
using UrbanStreetz.Models.UtilityModels;
using UrbanStreetz.Models.ViewModels;
using UrbanStreetz.Repositories.Interfaces;
using UrbanStreetz.Services.Interfaces;
using UrbanStreetz.Utilities;
using UrbanStreetz.Utilities.Extentions;

namespace UrbanStreetz.Services
{
    public class GroupService : IGroupService
    {
        private readonly IGroupRepository _groupRepository;
        private readonly IMapper _mapper;
        private readonly IConfigurationProvider _configuration;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public GroupService(IGroupRepository groupRepository, IMapper mapper, IConfigurationProvider configuration, IHttpContextAccessor httpContextAccessor)
        {
            _groupRepository=groupRepository;
            _mapper=mapper;
            _configuration=configuration;
            _httpContextAccessor=httpContextAccessor;
        }

        public StandardResponse<GroupView> CreateGroup(GroupModel model)
        {
            try
            {
                var loggedOnUserId = _httpContextAccessor.HttpContext.User.GetLoggedInUserId<int>();

                var mappedGroup = _mapper.Map<Group>(model);
                mappedGroup.CreatorUserId = loggedOnUserId;
                var createdGroup = _groupRepository.CreateAndReturn(mappedGroup);

                if(createdGroup == null)
                    StandardResponse<GroupView>.Failed(StandardResponseMessages.ERROR_OCCURRED);

                var newGroupUser = new GroupUser {
                    GroupId = createdGroup.Id,
                    IsAdmin = true,
                    UserId = loggedOnUserId
                };

                _groupRepository.CreateGroupUser(newGroupUser);

                return StandardResponse<GroupView>.Ok(_mapper.Map<GroupView>(createdGroup));
            }
            catch (Exception ex)
            {
                return StandardResponse<GroupView>.Failed(ex.Message);
            }
        }

        public StandardResponse<GroupView> UpdateGroup(GroupModel model)
        {
            try
            {
                var loggedOnUserId = _httpContextAccessor.HttpContext.User.GetLoggedInUserId<int>();

                var existingGroup = _groupRepository.AllGroups()
                    .Where(group => group.Id == model.Id && group.CreatorUserId == loggedOnUserId)
                    .FirstOrDefault();

                if (existingGroup == null)
                    return StandardResponse<GroupView>.Failed(StandardResponseMessages.ERROR_OCCURRED);

                existingGroup.Name = model.Name;
                existingGroup.Description = model.Description;
                existingGroup.DateModified= DateTime.Now;

                var updatedGroup = _groupRepository.Update(existingGroup);

                return updatedGroup == null
                    ? StandardResponse<GroupView>.Failed(StandardResponseMessages.ERROR_OCCURRED)
                    : StandardResponse<GroupView>.Ok(_mapper.Map<GroupView>(updatedGroup));
            }
            catch (Exception ex)
            {
                return StandardResponse<GroupView>.Failed(ex.Message);
            }
        }
        
        public StandardResponse<string> DeleteGroup(int id)
        {
            try
            {
                var existingGroup = _groupRepository.AllGroups()
                    .Where(group => group.Id == id)
                    .FirstOrDefault();

                if (existingGroup == null)
                    return StandardResponse<string>.Failed(StandardResponseMessages.ERROR_OCCURRED);

                existingGroup.IsDeleted = true;
                existingGroup.DateModified= DateTime.Now;

                var updatedGroup = _groupRepository.Update(existingGroup);

                return updatedGroup == null
                    ? StandardResponse<string>.Failed(StandardResponseMessages.ERROR_OCCURRED)
                    : StandardResponse<string>.Ok(StandardResponseMessages.OK);
            }
            catch (Exception ex)
            {
                return StandardResponse<string>.Failed(ex.Message);
            }
        }

        public StandardResponse<PagedCollection<GroupView>> AllGroups(PagingOptions pagingOptions)
        {
            try
            {
                var groups = _groupRepository.AllGroups()
                    .Take(50)
                    .AsQueryable()
                    .ProjectTo<GroupView>(_configuration)
                    .AsEnumerable();          

                var pagedGroups = groups.Skip(pagingOptions.Offset.Value).Take(pagingOptions.Limit.Value);
                var pagedResponse = PagedCollection<GroupView>.Create(
                    Link.ToCollection(nameof(GroupController.AllGroups)),
                    pagedGroups.ToArray(),
                    groups.Count(),
                    pagingOptions);

                return StandardResponse<PagedCollection<GroupView>>.Ok(pagedResponse);
            }
            catch (Exception ex)
            {
                return StandardResponse<PagedCollection<GroupView>>.Failed(ex.Message);
            }
        }

        public StandardResponse<PagedCollection<GroupView>> AllUserGroups(int userId, PagingOptions pagingOptions)
        {
            try
            {
                var groups = _groupRepository.ListUserGroup(userId)
                    .Take(50)
                    .AsQueryable()
                    .ProjectTo<GroupView>(_configuration)
                    .AsEnumerable();          

                var pagedGroups = groups.Skip(pagingOptions.Offset.Value).Take(pagingOptions.Limit.Value);
                var pagedResponse = PagedCollection<GroupView>.Create(
                    Link.ToCollection(nameof(GroupController.UserGroups)),
                    pagedGroups.ToArray(),
                    groups.Count(),
                    pagingOptions);

                return StandardResponse<PagedCollection<GroupView>>.Ok(pagedResponse);
            }
            catch (Exception ex)
            {
                return StandardResponse<PagedCollection<GroupView>>.Failed(ex.Message);
            }
        }
    }
}
