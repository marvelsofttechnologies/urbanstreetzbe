﻿using System;
using System.Linq;

using AutoMapper;
using AutoMapper.QueryableExtensions;

using Microsoft.AspNetCore.Http;
using UrbanStreetz.Controllers;
using UrbanStreetz.Models;
using UrbanStreetz.Models.AppModels;
using UrbanStreetz.Models.InputModels;
using UrbanStreetz.Models.UtilityModels;
using UrbanStreetz.Models.ViewModels;
using UrbanStreetz.Repositories.Interfaces;
using UrbanStreetz.Services.Interfaces;
using UrbanStreetz.Utilities;
using UrbanStreetz.Utilities.Extentions;

namespace UrbanStreetz.Services {
    public class HighlightService : IHighlightService {
        private readonly IHighlightRepository _highlightRepository;
        private readonly IStoryRepository _storyRepository;
        private readonly IMapper _mapper;
        public readonly IConfigurationProvider _configuration;
        private readonly IMediaRepository _mediaRepository;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public HighlightService(
            IHighlightRepository highlightRepository,
            IStoryRepository storeRepository,
            IMapper mapper,
            IConfigurationProvider configuration,
            IMediaRepository mediaRepository,
            IHttpContextAccessor httpContextAccessor
        ) {
            _highlightRepository = highlightRepository;
            _storyRepository = storeRepository;
            _mapper = mapper;
            _configuration = configuration;
            _mediaRepository = mediaRepository;
            _httpContextAccessor = httpContextAccessor;
        }

        public StandardResponse<PagedCollection<HighlightView>> ListHighlights(PagingOptions pagingOptions) {
            try {
                int loggedInUserId = _httpContextAccessor.HttpContext.User.GetLoggedInUserId<int>();

                var userHighlights = _highlightRepository.ListHighlights(loggedInUserId).AsQueryable()
                                                         .ProjectTo<HighlightView>(_configuration).AsEnumerable();

                var pagedUserHighlights = userHighlights.Skip(pagingOptions.Offset.Value).Take(pagingOptions.Limit.Value);
                var pagedResponse = PagedCollection<HighlightView>.Create(
                    Link.ToCollection(nameof(HighlightController.ListHighlights)),
                    pagedUserHighlights.ToArray(),
                    userHighlights.Count(),
                    pagingOptions);

                return StandardResponse<PagedCollection<HighlightView>>.Ok(pagedResponse);
            }
            catch (Exception ex) {
                Logger.Error(ex);

                return StandardResponse<PagedCollection<HighlightView>>.Failed(ex.Message);
            }
        }

        public StandardResponse<PagedCollection<StoryView>> ListHighlightStories(
            int highlightId,
            PagingOptions pagingOptions
        ) {
            try {
                int loggedInUserId = _httpContextAccessor.HttpContext.User.GetLoggedInUserId<int>();

                var highlightStories = _highlightRepository.ListHighlights(loggedInUserId)
                                                           .Single(highlight => highlight.Id == highlightId)
                                                           .Stories.AsQueryable().ProjectTo<StoryView>(_configuration)
                                                           .AsEnumerable();

                var pagedHighlightStories
                    = highlightStories.Skip(pagingOptions.Offset.Value).Take(pagingOptions.Limit.Value);

                var pagedResponse = PagedCollection<StoryView>.Create(
                    Link.ToCollection(nameof(HighlightController.ListHighlightStories)),
                    pagedHighlightStories.ToArray(), highlightStories.Count(), pagingOptions);

                return StandardResponse<PagedCollection<StoryView>>.Ok(pagedResponse);
            }
            catch (Exception ex) {
                Logger.Error(ex.Message);

                return StandardResponse<PagedCollection<StoryView>>.Failed(ex.Message);
            }
        }

        public StandardResponse<HighlightView> AddStoryToHighlight(HighlightModel model) {
            try {
                int loggedInUserId = _httpContextAccessor.HttpContext.User.GetLoggedInUserId<int>();

                var highlight = _highlightRepository.ListHighlights(loggedInUserId).FirstOrDefault(h => h.Id == model.Id);

                if (highlight == null) {
                    var newHighlight = new Highlight { Name = model.Name, CreatorUserId = loggedInUserId };
                    highlight = _highlightRepository.CreateAndReturn(newHighlight);
                }

                var story = _storyRepository.GetById(model.StoryId);
                story.HighlightId = highlight.Id;
                _storyRepository.Update(story);

                var mappedHighlight = _mapper.Map<HighlightView>(highlight);
                return StandardResponse<HighlightView>.Ok(mappedHighlight);
            }
            catch (Exception ex) {
                Logger.Error(ex.Message);

                return StandardResponse<HighlightView>.Failed(ex.Message);
            }
        }

        public StandardResponse<bool> DeleteHighlightStory(int id) {
            try {
                var story = _storyRepository.GetById(id);
                story.HighlightId = null;

                var result = _storyRepository.Update(story);

                return StandardResponse<bool>.Ok(result != null);
            }
            catch (Exception ex) {
                Logger.Error(ex.Message);

                return StandardResponse<bool>.Failed(ex.Message);
            }
        }
        
        public StandardResponse<bool> DeleteHighlight(int id) {
            try {
                var highlight = _highlightRepository.GetById(id);

                bool storyResult = _highlightRepository.RemoveAllStories(highlight.Id);

                if (!storyResult) return StandardResponse<bool>.Failed(StandardResponseMessages.ERROR_OCCURRED);
                
                _highlightRepository.Delete(highlight);

                return StandardResponse<bool>.Ok(true);
            }
            catch (Exception ex) {
                Logger.Error(ex.Message);

                return StandardResponse<bool>.Failed(ex.Message);
            }
        }
    }
}
