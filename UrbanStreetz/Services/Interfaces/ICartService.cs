﻿using UrbanStreetz.Models.InputModels;
using UrbanStreetz.Models.UtilityModels;
using UrbanStreetz.Models.ViewModels;
using UrbanStreetz.Utilities;

namespace UrbanStreetz.Services.Interfaces {
    public interface ICartService {
        StandardResponse<CartView> CreateCart(CartModel model);
        StandardResponse<bool> RemoveCart(int id);
        StandardResponse<PagedCollection<CartView>> ListCart(PagingOptions pagingOptions);
    }
}
