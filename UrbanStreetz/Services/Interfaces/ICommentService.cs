using System.Collections.Generic;
using UrbanStreetz.Models.InputModels;
using UrbanStreetz.Models.UtilityModels;
using UrbanStreetz.Models.ViewModels;
using UrbanStreetz.Utilities;

namespace UrbanStreetz.Services.Interfaces
{
    public interface ICommentService
    {
        StandardResponse<PagedCollection<CommentView>> ListPostComments(int PostId, PagingOptions pagingOptions);
        StandardResponse<CommentView> New(CommentModel comment);
        StandardResponse<PagedCollection<UserView>> ListCommentLikeUsers(int commentId, PagingOptions pagingOptions);
    }
}