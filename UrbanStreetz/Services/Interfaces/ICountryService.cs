using UrbanStreetz.Models.UtilityModels;
using UrbanStreetz.Models.ViewModels;
using UrbanStreetz.Utilities;

namespace UrbanStreetz.Services.Interfaces {
    public interface ICountryService
    {
        StandardResponse<PagedCollection<CountryView>> ListCountries(PagingOptions pagingOptions);
        StandardResponse<PagedCollection<StateView>> ListStates(int countryId, PagingOptions pagingOptions);
        StandardResponse<PagedCollection<BaseNameModelView>> ListCities(int stateId, PagingOptions pagingOptions);
    }
}