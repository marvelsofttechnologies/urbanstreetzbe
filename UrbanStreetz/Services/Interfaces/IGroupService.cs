﻿using System.Collections.Generic;
using UrbanStreetz.Models.InputModels;
using UrbanStreetz.Models.UtilityModels;
using UrbanStreetz.Models.ViewModels;
using UrbanStreetz.Utilities;

namespace UrbanStreetz.Services.Interfaces
{
    public interface IGroupService
    {
        StandardResponse<GroupView> CreateGroup(GroupModel model);
        StandardResponse<GroupView> UpdateGroup(GroupModel model);
        StandardResponse<string> DeleteGroup(int id);
        StandardResponse<PagedCollection<GroupView>> AllGroups(PagingOptions pagingOptions);
        StandardResponse<PagedCollection<GroupView>> AllUserGroups(int userId, PagingOptions pagingOptions);
    }
}
