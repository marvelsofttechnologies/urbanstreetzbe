﻿using UrbanStreetz.Models.InputModels;
using UrbanStreetz.Models.UtilityModels;
using UrbanStreetz.Models.ViewModels;
using UrbanStreetz.Utilities;

namespace UrbanStreetz.Services.Interfaces {
    public interface IHighlightService {
        StandardResponse<PagedCollection<HighlightView>> ListHighlights(PagingOptions pagingOptions);
        StandardResponse<PagedCollection<StoryView>> ListHighlightStories(int highlightId, PagingOptions pagingOptions);
        StandardResponse<HighlightView> AddStoryToHighlight(HighlightModel model);
        StandardResponse<bool> DeleteHighlightStory(int id);
        StandardResponse<bool> DeleteHighlight(int id);
    }
}
