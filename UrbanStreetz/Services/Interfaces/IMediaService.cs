using UrbanStreetz.Models.AppModels;
using UrbanStreetz.Models.InputModels;
using UrbanStreetz.Models.ViewModels;
using UrbanStreetz.Utilities;

namespace UrbanStreetz.Services.Interfaces
{
    public interface IMediaService
    {
         StandardResponse<Media> UploadMedia(Models.InputModels.MediaModel media);
         StandardResponse<MediaView> GetById(int id);
         StandardResponse<MediaView> Delete(int id);
    }
}