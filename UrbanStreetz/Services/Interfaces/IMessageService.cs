﻿using UrbanStreetz.Models.UtilityModels;
using UrbanStreetz.Models.ViewModels;
using UrbanStreetz.Utilities;

namespace UrbanStreetz.Services.Interfaces
{
    public interface IMessageService
    {
        StandardResponse<string> DeleteMessage(int id);
        StandardResponse<PagedCollection<MessageView>> MessageOverview(PagingOptions pagingOptions);
        StandardResponse<PagedCollection<MessageView>> ChatMessages(int recipientId, PagingOptions pagingOptions);
        StandardResponse<PagedCollection<MessageView>> GroupMessages(int groupId, PagingOptions pagingOptions);
    }
}
