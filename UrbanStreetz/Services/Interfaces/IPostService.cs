using UrbanStreetz.Utilities;
using UrbanStreetz.Models.ViewModels;
using System.Collections.Generic;
using UrbanStreetz.Models.InputModels;
using UrbanStreetz.Models.UtilityModels;

namespace UrbanStreetz.Services.Interfaces
{
    public interface IPostService
    {
        StandardResponse<PostView> CreatePost(PostModel model);
        StandardResponse<bool> Dislike(int postId);
        StandardResponse<bool> Like(int postId);
        StandardResponse<PagedCollection<PostCategoryView>> ListPostCategories(PagingOptions pagingOptions);
        StandardResponse<PagedCollection<PostView>> ListPosts(string searchQuery, PagingOptions pagingOptions);
        StandardResponse<PostView> GetPost(int postId);
        StandardResponse<PagedCollection<PostView>> RelatedPosts(int postId, PagingOptions pagingOptions);
        StandardResponse<PagedCollection<PostView>> PopularPosts(string searchQuery, PagingOptions pagingOptions);
        StandardResponse<PagedCollection<PostView>> ListPersonalizedPosts(string searchQuery, PagingOptions pagingOptions);
        StandardResponse<PagedCollection<UserView>> ListPostLikeUsers(int postId, PagingOptions pagingOptions);
    }
}