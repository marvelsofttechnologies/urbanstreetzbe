using System.Collections.Generic;
using System.Threading.Tasks;
using UrbanStreetz.Models.InputModels;
using UrbanStreetz.Models.UtilityModels;
using UrbanStreetz.Models.ViewModels;
using UrbanStreetz.Utilities;

namespace UrbanStreetz.Services.Interfaces
{
    public interface IStoryService
    {
        StandardResponse<PagedCollection<StoryGroupView>> ListFollowerStories(PagingOptions pagingOptions);
        Task<StandardResponse<StoryView>> CreateStory(StoryModel model);
        StandardResponse<PagedCollection<StoryView>> ListUserStories(PagingOptions pagingOptions);
        StandardResponse<StoryView> GetStory(int storyId);
        StandardResponse<string> DeleteStory(int storyId);
    }
}