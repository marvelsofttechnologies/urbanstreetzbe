﻿using System;
using System.Collections.Generic;

using UrbanStreetz.Models.InputModels;
using UrbanStreetz.Models.UtilityModels;
using UrbanStreetz.Models.ViewModels;
using UrbanStreetz.Utilities;

namespace UrbanStreetz.Services.Abstractions
{
    public interface IUserService 
    { 
        StandardResponse<UserView> CreateUser(UserModel model);
        StandardResponse<UserView> Authenticate(LoginModel userToLogin);
        public StandardResponse<UserView> VerifyUser(string token, string email);
        StandardResponse<UserView> InitiatePasswordReset(string email);
        StandardResponse<UserView> CompletePasswordReset(PasswordReset payload);
        StandardResponse<UserView> UpdateUser(UpdateUserModel model);
        StandardResponse<bool> Follow(int UserId);
        StandardResponse<bool> UnFollow(int UserId);
        StandardResponse<UserProfileView> UserProfile(int userId);
        StandardResponse<PagedCollection<UserView>> ListMyFollowers(PagingOptions pagingOptions);
    }
}

