using System;
using AutoMapper;
using UrbanStreetz.Models.AppModels;
using UrbanStreetz.Models.InputModels;
using UrbanStreetz.Repositories.Interfaces;
using UrbanStreetz.Services.Interfaces;
using UrbanStreetz.Utilities;
using UrbanStreetz.Utilities.Abstrctions;
using UrbanStreetz.Utilities.Constants;
using UrbanStreetz.Models.ViewModels;

namespace UrbanStreetz.Services
{
    public class MediaService : IMediaService
    {
        private readonly IMapper _mapper;
        private readonly ICodeProvider _codeProvider;
        private readonly IMediaRepository _mediaRepository;

        public MediaService(IMapper mapper, ICodeProvider codeProvider, IMediaRepository mediaRepository)
        {
            _mapper = mapper;
            _codeProvider = codeProvider;
            _mediaRepository = mediaRepository;
        }

        public StandardResponse<Media> UploadMedia(MediaModel media)
        {
            try
            {
                media.Name = _codeProvider.New(0,Constants.NEW_PROPERTY_MEDIA_NAME,0,10,Constants.PROPERTY_MATAAZ_MEDIA_PREFIX).CodeString;

                Media NewMedia = _mapper.Map<Media>(media);

                var Result = _mediaRepository.UploadMedia(NewMedia).Result;

                if(!Result.Succeeded)
                    return StandardResponse<Media>.Ok().AddStatusMessage(StandardResponseMessages.MEDIA_UPLOAD_FAILED);

                return StandardResponse<Media>.Ok().AddData(Result.UploadedMedia).AddStatusMessage(StandardResponseMessages.MEDIA_UPLOAD_SUCCESSFUL);
            }
            catch (Exception e)
            {
                return StandardResponse<Media>.Error(StandardResponseMessages.ERROR_OCCURRED);
            }
        }

        public StandardResponse<MediaView> GetById(int id){
            try
            {
                return StandardResponse<MediaView>.Ok(_mapper.Map<MediaView>(_mediaRepository.GetById(id)));
            }
            catch (Exception e)
            {
                Logger.Error(e);
                return StandardResponse<MediaView>.Error(StandardResponseMessages.ERROR_OCCURRED);
            }
        }

        public StandardResponse<MediaView> Delete(int id){
            try{
                var mediaToDelete = _mediaRepository.GetById(id);
                if(mediaToDelete == null)
                    return StandardResponse<MediaView>.Failed();

                _mediaRepository.Delete(mediaToDelete);
                return StandardResponse<MediaView>.Ok();
            }
            catch (Exception e)
            {
                Logger.Error(e);
                return StandardResponse<MediaView>.Failed();
            }
        }
    }
}