﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using System.Linq;
using System;
using UrbanStreetz.Models.InputModels;
using UrbanStreetz.Models.ViewModels;
using UrbanStreetz.Repositories.Interfaces;
using UrbanStreetz.Services.Interfaces;
using UrbanStreetz.Utilities.Extentions;
using UrbanStreetz.Utilities;
using UrbanStreetz.Models.AppModels;
using System.Collections.Generic;
using UrbanStreetz.Repositories;
using AutoMapper.QueryableExtensions;
using UrbanStreetz.Models.UtilityModels;
using UrbanStreetz.Controllers;
using UrbanStreetz.Models;
using System.Net.NetworkInformation;

namespace UrbanStreetz.Services
{
    public class MessageService : IMessageService
    {
        private readonly IMessageRepository _messageRepository;
        private readonly IMapper _mapper;
        private readonly IConfigurationProvider _configuration;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public MessageService(IMessageRepository messageRepository, IMapper mapper, IConfigurationProvider configuration, IHttpContextAccessor httpContextAccessor)
        {
            _messageRepository=messageRepository;
            _mapper=mapper;
            _configuration=configuration;
            _httpContextAccessor=httpContextAccessor;
        }

        //public StandardResponse<MessageView> CreateMessage(MessageModel model)
        //{
        //    try
        //    {
        //        var mappedMessage = _mapper.Map<Message>(model);
        //        mappedMessage.SenderId = _httpContextAccessor.HttpContext.User.GetLoggedInUserId<int>();
        //        var createdMessage = _messageRepository.CreateAndReturn(mappedMessage);

        //        return createdMessage == null
        //            ? StandardResponse<MessageView>.Failed(StandardResponseMessages.ERROR_OCCURRED)
        //            : StandardResponse<MessageView>.Ok(_mapper.Map<MessageView>(createdMessage));
        //    }
        //    catch (Exception ex)
        //    {
        //        return StandardResponse<MessageView>.Failed(ex.Message);
        //    }
        //}



        public StandardResponse<string> DeleteMessage(int id)
        {
            try
            {
                var loggedOnUserId = _httpContextAccessor.HttpContext.User.GetLoggedInUserId<int>();

                var existingMessage = _messageRepository.GetAll()
                    .Where(message => message.Id == id && message.SenderId ==loggedOnUserId)
                    .FirstOrDefault();

                if (existingMessage == null)
                    return StandardResponse<string>.Failed(StandardResponseMessages.ERROR_OCCURRED);

                existingMessage.IsDeleted = true;
                existingMessage.DateModified= DateTime.Now;

                var updatedMessage = _messageRepository.Update(existingMessage);

                return updatedMessage == null
                    ? StandardResponse<string>.Failed(StandardResponseMessages.ERROR_OCCURRED)
                    : StandardResponse<string>.Ok(StandardResponseMessages.OK);
            }
            catch (Exception ex)
            {
                return StandardResponse<string>.Failed(ex.Message);
            }
        }

        public StandardResponse<PagedCollection<MessageView>> MessageOverview(PagingOptions pagingOptions) {
            
            try
            {
                int loggedOnUserId = _httpContextAccessor.HttpContext.User.GetLoggedInUserId<int>();

                var messages = from m in _messageRepository.ListMessages()
                               let msgTo = m.RecipientId == loggedOnUserId
                               let msgFrom = m.SenderId == loggedOnUserId
                               where msgTo || msgFrom
                               group m by m.GroupId.HasValue 
                                    ? m.GroupId 
                                    : msgTo 
                                        ? m.RecipientId 
                                        : m.SenderId 
                               into g
                               select g.OrderByDescending(x => x.DateCreated)
                               .AsQueryable()
                               .ProjectTo<MessageView>(_configuration)
                               .First();

                var pagedMessages = messages.Skip(pagingOptions.Offset.Value).Take(pagingOptions.Limit.Value);
                var pagedResponse = PagedCollection<MessageView>.Create(
                    Link.ToCollection(nameof(MessageController.MessageOverview)),
                    pagedMessages.ToArray(),
                    messages.Count(),
                    pagingOptions);

                return StandardResponse<PagedCollection<MessageView>>.Ok(pagedResponse);
            }
            catch (Exception ex)
            {
                return StandardResponse<PagedCollection<MessageView>>.Failed(ex.Message);
            }
        }

        public StandardResponse<PagedCollection<MessageView>> ChatMessages(int recipientId, PagingOptions pagingOptions)
        {
            try
            {
                var loggedOnUserId = _httpContextAccessor.HttpContext.User.GetLoggedInUserId<int>();

                var messages = _messageRepository.ChatMessages(loggedOnUserId, recipientId)
                    .AsQueryable()
                    .ProjectTo<MessageView>(_configuration)
                    .AsEnumerable();

                var pagedMessages = messages.Skip(pagingOptions.Offset.Value).Take(pagingOptions.Limit.Value);
                var pagedResponse = PagedCollection<MessageView>.Create(
                    Link.ToCollection(nameof(MessageController.ChatMessages)),
                    pagedMessages.ToArray(),
                    messages.Count(),
                    pagingOptions);

                return StandardResponse<PagedCollection<MessageView>>.Ok(pagedResponse);
            }
            catch (Exception ex)
            {
                return StandardResponse<PagedCollection<MessageView>>.Failed(ex.Message);
            }
        }

        public StandardResponse<PagedCollection<MessageView>> GroupMessages(int groupId, PagingOptions pagingOptions)
        {
            try
            {
                var loggedOnUserId = _httpContextAccessor.HttpContext.User.GetLoggedInUserId<int>();

                var messages = _messageRepository.GroupMessages(groupId)
                    
                    .AsQueryable()
                    .ProjectTo<MessageView>(_configuration)
                    .AsEnumerable();

                var pagedMessages = messages.Skip(pagingOptions.Offset.Value).Take(pagingOptions.Limit.Value);
                var pagedResponse = PagedCollection<MessageView>.Create(
                    Link.ToCollection(nameof(MessageController.GroupMessages)),
                    pagedMessages.ToArray(),
                    messages.Count(),
                    pagingOptions);

                return StandardResponse<PagedCollection<MessageView>>.Ok(pagedResponse);
            }
            catch (Exception ex)
            {
                return StandardResponse<PagedCollection<MessageView>>.Failed(ex.Message);
            }
        }
    }
}
