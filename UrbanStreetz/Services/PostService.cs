using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Hosting;
using UrbanStreetz.Controllers;
using UrbanStreetz.Models;
using UrbanStreetz.Models.AppModels;
using UrbanStreetz.Models.InputModels;
using UrbanStreetz.Models.UtilityModels;
using UrbanStreetz.Models.ViewModels;
using UrbanStreetz.Repositories.Interfaces;
using UrbanStreetz.Services.Interfaces;
using UrbanStreetz.Utilities;
using UrbanStreetz.Utilities.Extentions;

namespace UrbanStreetz.Services {
    public class PostService : IPostService {
        private readonly IPostRepository _postRepository;
        private readonly IMapper _mapper;
        private readonly IConfigurationProvider _configuration;
        private readonly IMediaRepository _mediaRepository;
        private readonly ILikeRepository _likeRepository;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IUserInterestRepository _userInterestRepository;

        public PostService(IPostRepository postRepository, IMapper mapper, IConfigurationProvider configuration, IMediaRepository mediaRepository, ILikeRepository likeRepository, IHttpContextAccessor httpContextAccessor, IUserInterestRepository userInterestRepository) {
            _postRepository = postRepository;
            _mapper = mapper;
            _configuration = configuration;
            _mediaRepository = mediaRepository;
            _likeRepository = likeRepository;
            _httpContextAccessor = httpContextAccessor;
            _userInterestRepository = userInterestRepository;
        }

        public StandardResponse<PagedCollection<PostCategoryView>> ListPostCategories(PagingOptions pagingOptions) {
            try {
                var postCategories = _postRepository
                    .ListPostCategories()
                    .AsQueryable()
                    .ProjectTo<PostCategoryView>(_configuration)
                    .AsEnumerable();

                var pagedPostCategories = postCategories.Skip(pagingOptions.Offset.Value).Take(pagingOptions.Limit.Value);
                var pagedResponse = PagedCollection<PostCategoryView>.Create(
                    Link.ToCollection(nameof(PostController.ListPostCategories)),
                    pagedPostCategories.ToArray(),
                    postCategories.Count(),
                    pagingOptions);

                return StandardResponse<PagedCollection<PostCategoryView>>.Ok(pagedResponse);
            }
            catch (Exception ex) {
                return StandardResponse<PagedCollection<PostCategoryView>>.Failed(ex.Message);
            }
        }

        public StandardResponse<PagedCollection<PostView>> ListPosts(string searchQuery, PagingOptions pagingOptions) {
            try {
                var posts = _postRepository
                            .ListPosts()
                            .AsQueryable()
                            .ProjectTo<PostView>(_configuration)
                            .AsEnumerable();

                if (!string.IsNullOrEmpty(searchQuery))
                    posts = posts.Where(post => post.Feed.ToLower().Contains(searchQuery.Trim().ToLower()) ||
                                                post.Description.ToLower().Contains(searchQuery.Trim().ToLower()) ||
                                                post.Brand.ToLower().Contains(searchQuery.Trim().ToLower()) ||
                                                post.PostCategory.ToLower().Contains(searchQuery.Trim().ToLower()) ||
                                                post.User.FullName.ToLower().Contains(searchQuery.Trim().ToLower()));

                var pagedPosts = posts.Skip(pagingOptions.Offset.Value).Take(pagingOptions.Limit.Value);
                var pagedResponse = PagedCollection<PostView>.Create(
                    Link.ToCollection(nameof(PostController.ListPosts)),
                    pagedPosts.ToArray(),
                    posts.Count(),
                    pagingOptions);

                return StandardResponse<PagedCollection<PostView>>.Ok(pagedResponse);
            }
            catch (Exception ex) {
                return StandardResponse<PagedCollection<PostView>>.Failed(ex.Message);
            }
        }

        public StandardResponse<PostView> GetPost(int postId) {
            try {
                var post = _postRepository.ListPosts()
                                          .FirstOrDefault(p => p.Id == postId);

                if (post == null) return StandardResponse<PostView>.Error(StandardResponseMessages.NOT_FOUND);

                var mappedPost = _mapper.Map<PostView>(post);

                return StandardResponse<PostView>.Ok(mappedPost);
            }
            catch (Exception ex) {
                Logger.Error(ex);

                return StandardResponse<PostView>.Failed(ex.Message);
            }
        }
        public StandardResponse<PostView> CreatePost(PostModel model) {
            try {
                var post = _mapper.Map<Post>(model);
                var postMedias = post.Medias;
                post.Medias = null;
                post.UserId = _httpContextAccessor.HttpContext.User.GetLoggedInUserId<int>();
                var createdPost = _postRepository.CreateAndReturn(post);
                if (createdPost != null) {
                    if (postMedias != null) {
                        foreach (var media in postMedias) {
                            media.PostId = createdPost.Id;
                            _mediaRepository.UploadMedia(media);
                        }
                    }
                }
                return StandardResponse<PostView>.Ok(_mapper.Map<PostView>(post));
            }
            catch (Exception ex) {
                return StandardResponse<PostView>.Failed(ex.Message);
            }
        }
        public StandardResponse<bool> Like(int postId) {
            try {
                var LoggedInUser = _httpContextAccessor.HttpContext.User.GetLoggedInUserId<int>();

                var thisPost = _postRepository.GetById(postId);
                var existLike = _likeRepository.GetAll()
                                               .FirstOrDefault(like => like.PostId == postId && like.UserId == LoggedInUser);

                if (existLike != null) {
                    if (existLike.IsDislike)
                        thisPost.NumberOfDislikes--;

                    existLike.IsLike = true;
                    existLike.IsDislike = false;
                    _likeRepository.Update(existLike);

                    thisPost.NumberOfDislikes++;
                    thisPost = _postRepository.Update(thisPost);
                    return StandardResponse<bool>.Ok(true);
                }

                existLike = new Like {
                    PostId = postId,
                    UserId = LoggedInUser,
                    CommentId = null,
                    IsLike = true,
                    IsDislike = false
                };
                var createdLike = _likeRepository.CreateAndReturn(existLike);
                thisPost.NumberOfLikes++;
                thisPost = _postRepository.Update(thisPost);
                return StandardResponse<bool>.Ok(true);
            }
            catch (Exception ex) {
                return StandardResponse<bool>.Failed(ex.Message);
            }

        }
        public StandardResponse<bool> Dislike(int postId) {
            try {
                var LoggedInUser = _httpContextAccessor.HttpContext.User.GetLoggedInUserId<int>();
                var thisPost = _postRepository.GetById(postId);

                var existLike = _likeRepository.GetAll()
                                               .FirstOrDefault(like => like.PostId == postId && like.UserId == LoggedInUser);

                if (existLike != null) {
                    if (existLike.IsLike)
                        thisPost.NumberOfLikes--;

                    existLike.IsLike = false;
                    existLike.IsDislike = true;
                    _likeRepository.Update(existLike);

                    thisPost.NumberOfDislikes++;
                    thisPost = _postRepository.Update(thisPost);
                    return StandardResponse<bool>.Ok(true);
                }

                existLike = new Like {
                    PostId = postId,
                    UserId = LoggedInUser,
                    CommentId = null,
                    IsLike = false,
                    IsDislike = true
                };
                var createdLike = _likeRepository.CreateAndReturn(existLike);
                thisPost.NumberOfDislikes++;
                thisPost = _postRepository.Update(thisPost);
                return StandardResponse<bool>.Ok(true);
            }
            catch (Exception ex) {
                return StandardResponse<bool>.Failed(ex.Message);
            }
        }

        public StandardResponse<PagedCollection<PostView>> RelatedPosts(int postId, PagingOptions pagingOptions) {
            try {
                var existingPost = _postRepository.GetById(postId);

                if (existingPost == null)
                    return StandardResponse<PagedCollection<PostView>>.Error(StandardResponseMessages.NOT_FOUND);

                var relatedPosts = _postRepository
                                  .ListPosts()
                                  .Where(post => post.PostCategoryId == existingPost.PostCategoryId ||
                                                 string.Equals(post.Brand, existingPost.Brand))
                                  .AsQueryable()
                                  .ProjectTo<PostView>(_configuration)
                                  .AsEnumerable();

                var pagedPosts = relatedPosts.Skip(pagingOptions.Offset.Value).Take(pagingOptions.Limit.Value);
                var pagedResponse = PagedCollection<PostView>.Create(
                    Link.ToCollection(nameof(PostController.ListRelatedPosts)),
                    pagedPosts.ToArray(),
                    relatedPosts.Count(),
                    pagingOptions);

                return StandardResponse<PagedCollection<PostView>>.Ok(pagedResponse);
            }
            catch (Exception ex) {
                return StandardResponse<PagedCollection<PostView>>.Failed(ex.Message);
            }
        }

        public StandardResponse<PagedCollection<PostView>> PopularPosts(string searchQuery, PagingOptions pagingOptions) {
            try {
                var ds = _postRepository
                        .ListPosts()
                        .OrderBy(post => post.Comments.Count())
                        .ThenBy(post => post.NumberOfLikes).ToList();

                var posts = _postRepository
                           .ListPosts()
                           .AsQueryable()
                           .ProjectTo<PostView>(_configuration)
                           .OrderBy(post => post.NumberOfLikes)
                           .AsEnumerable();
                //.ThenBy(post => post.Comments.Count());

                if (!string.IsNullOrEmpty(searchQuery))
                    posts = posts.Where(post => post.Feed.ToLower().Contains(searchQuery.Trim().ToLower()) ||
                                                post.Description.ToLower().Contains(searchQuery.Trim().ToLower()) ||
                                                post.Brand.ToLower().Contains(searchQuery.Trim().ToLower()) ||
                                                post.PostCategory.ToLower().Contains(searchQuery.Trim().ToLower()) ||
                                                post.User.FullName.ToLower().Contains(searchQuery.Trim().ToLower()));

                
                var pagedPosts = posts.Skip(pagingOptions.Offset.Value).Take(pagingOptions.Limit.Value);
                var pagedResponse = PagedCollection<PostView>.Create(
                    Link.ToCollection(nameof(PostController.ListPopularPosts)),
                    pagedPosts.ToArray(),
                    posts.Count(),
                    pagingOptions);


                return StandardResponse<PagedCollection<PostView>>.Ok(pagedResponse);
            }
            catch (Exception ex) {
                Logger.Error(ex);

                return StandardResponse<PagedCollection<PostView>>.Failed(ex.Message);
            }
        }

        public StandardResponse<PagedCollection<PostView>> ListPersonalizedPosts(string searchQuery, PagingOptions pagingOptions) {
            try {
                int userId = _httpContextAccessor.HttpContext.User.GetLoggedInUserId<int>();

                var userInterestIds = _userInterestRepository.GetAll()
                                                             .Where(user => user.UserId == userId)
                                                             .Select(interest => interest.PostCategoryId);

                var posts = _postRepository
                           .ListPosts()
                           .Where(post => userInterestIds.Any(interest => interest == post.PostCategoryId))
                           .AsQueryable()
                           .ProjectTo<PostView>(_configuration)
                           .AsEnumerable();

                if (!string.IsNullOrEmpty(searchQuery))
                    posts = posts.Where(post => post.Feed.ToLower().Contains(searchQuery.Trim().ToLower()) ||
                                                post.Description.ToLower().Contains(searchQuery.Trim().ToLower()) ||
                                                post.Brand.ToLower().Contains(searchQuery.Trim().ToLower()) ||
                                                post.PostCategory.ToLower().Contains(searchQuery.Trim().ToLower()) ||
                                                post.User.FullName.ToLower().Contains(searchQuery.Trim().ToLower()));

                
                var pagedPosts = posts.Skip(pagingOptions.Offset.Value).Take(pagingOptions.Limit.Value);
                var pagedResponse = PagedCollection<PostView>.Create(
                    Link.ToCollection(nameof(PostController.ListInterestedPosts)),
                    pagedPosts.ToArray(),
                    posts.Count(),
                    pagingOptions);

                return StandardResponse<PagedCollection<PostView>>.Ok(pagedResponse);
            }
            catch (Exception ex) {
                return StandardResponse<PagedCollection<PostView>>.Failed(ex.Message);
            }
        }

        public StandardResponse<PagedCollection<UserView>> ListPostLikeUsers(int postId, PagingOptions pagingOptions) {
            try {
                var postLikedUsers = _likeRepository.ListLikes()
                                                    .Where(like => like.PostId == postId && like.IsLike)
                                                    .Select(like => like.User)
                                                    .AsQueryable()
                                                    .ProjectTo<UserView>(_configuration)
                                                    .AsEnumerable();

                
                var pagedUsers = postLikedUsers.Skip(pagingOptions.Offset.Value).Take(pagingOptions.Limit.Value);
                var pagedResponse = PagedCollection<UserView>.Create(
                    Link.ToCollection(nameof(PostController.ListPostLikeUsers)),
                    pagedUsers.ToArray(),
                    postLikedUsers.Count(),
                    pagingOptions);

                return StandardResponse<PagedCollection<UserView>>.Ok(pagedResponse);
            }
            catch (Exception ex) {
                Logger.Error(ex);

                return StandardResponse<PagedCollection<UserView>>.Failed(ex.Message);
            }
        }

    }
}