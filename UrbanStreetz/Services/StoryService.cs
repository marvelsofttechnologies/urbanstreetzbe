using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UrbanStreetz.Controllers;
using UrbanStreetz.Models;
using UrbanStreetz.Models.AppModels;
using UrbanStreetz.Models.InputModels;
using UrbanStreetz.Models.UtilityModels;
using UrbanStreetz.Models.ViewModels;
using UrbanStreetz.Repositories.Interfaces;
using UrbanStreetz.Services.Interfaces;
using UrbanStreetz.Utilities;
using UrbanStreetz.Utilities.Extentions;

namespace UrbanStreetz.Services
{
    public class StoryService : IStoryService
    {
        private readonly IStoryRepository _storyRepository;
        private readonly IMapper _mapper;
        public readonly IConfigurationProvider _configuration;
        private readonly IMediaRepository _mediaRepository;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IUserFollowerRepository _userFollowerRepository;

        public StoryService(IStoryRepository storyRepository, IMapper mapper, IConfigurationProvider configuration, IMediaRepository mediaRepository, ILikeRepository likeRepository, IHttpContextAccessor httpContextAccessor, IUserFollowerRepository userFollowerRepository)
        {
            _storyRepository = storyRepository;
            _mapper = mapper;
            _configuration = configuration;
            _mediaRepository = mediaRepository;
            _httpContextAccessor = httpContextAccessor;
            _userFollowerRepository =   userFollowerRepository;
        }

        public StandardResponse<PagedCollection<StoryGroupView>> ListFollowerStories(PagingOptions pagingOptions)
        {
            try
            {
                var loggedOnUserId = _httpContextAccessor.HttpContext.User.GetLoggedInUserId<int>();

                var userFollowerIds = _userFollowerRepository.GetMyFollowings(loggedOnUserId)
                    .Select(followers => followers.UserId);

                var followerStories = _storyRepository.ListStories()
                    .Where(story => userFollowerIds.Any(followers => followers == story.UserId))
                    .GroupBy(story => story.UserId)
                    .Select(storyGroup => new StoryGroup { UserId = storyGroup.Key, Story = storyGroup })
                    .AsQueryable()
                    .ProjectTo<StoryGroupView>(_configuration)
                    .AsEnumerable();

                var pagedStories = followerStories.Skip(pagingOptions.Offset.Value).Take(pagingOptions.Limit.Value);
                var pagedResponse = PagedCollection<StoryGroupView>.Create(
                    Link.ToCollection(nameof(StoryController.ListFollowersStories)),
                    pagedStories.ToArray(),
                    followerStories.Count(),
                    pagingOptions);

                return StandardResponse<PagedCollection<StoryGroupView>>.Ok(pagedResponse);
            }
            catch (Exception ex)
            {
                return StandardResponse<PagedCollection<StoryGroupView>>.Failed(ex.Message);
            }
        }

        public StandardResponse<PagedCollection<StoryView>> ListUserStories(PagingOptions pagingOptions)
        {
            try
            {
                var loggedOnUserId = _httpContextAccessor.HttpContext.User.GetLoggedInUserId<int>();
                
                var userStories = _storyRepository.ListStories()
                    .Where(story => story.UserId == loggedOnUserId)
                    .AsQueryable()
                    .ProjectTo<StoryView>(_configuration)
                    .AsEnumerable();

                var pagedStories = userStories.Skip(pagingOptions.Offset.Value).Take(pagingOptions.Limit.Value);
                var pagedResponse = PagedCollection<StoryView>.Create(
                    Link.ToCollection(nameof(StoryController.ListUserStories)),
                    pagedStories.ToArray(),
                    userStories.Count(),
                    pagingOptions);

                return StandardResponse<PagedCollection<StoryView>>.Ok(pagedResponse);
            }
            catch (Exception ex)
            {
                return StandardResponse<PagedCollection<StoryView>>.Failed(ex.Message);
            }
        }

        public async Task<StandardResponse<StoryView>> CreateStory(StoryModel model)
        {
            try
            {
                var story = _mapper.Map<Story>(model);
                story.UserId = _httpContextAccessor.HttpContext.User.GetLoggedInUserId<int>();
                story.ExpiryDate = DateTime.Now.AddHours(24);

                // create story media
                if (story.Media != null)
                {
                    var (Succeeded, UploadedMedia)= await _mediaRepository.UploadMedia(story.Media);
                    if (Succeeded)
                    {
                        story.MediaId = UploadedMedia.Id;
                        story.Media = UploadedMedia;
                    }
                    else
                    {
                        return StandardResponse<StoryView>.Failed(StandardResponseMessages.ERROR_OCCURRED);
                    }
                }

                var createdStory = _storyRepository.CreateAndReturn(story);

                return StandardResponse<StoryView>.Ok(_mapper.Map<StoryView>(story));
            }
            catch (Exception ex)
            {
                return StandardResponse<StoryView>.Failed(ex.Message);
            }
        }

        public StandardResponse<StoryView> GetStory(int storyId)
        {
            try
            {
                var loggedOnUser = _httpContextAccessor.HttpContext.User.GetLoggedInUserId<int>();

                var userFollowerIds = _userFollowerRepository.GetMyFollowings(loggedOnUser)
                   .Select(followers => followers.UserId);

                var story = _storyRepository.ListStories()
                    .FirstOrDefault(story => story.Id == storyId && (story.UserId == loggedOnUser || userFollowerIds.Any(followers => followers == story.UserId)));

                if (story == null)
                    return StandardResponse<StoryView>.Failed(StandardResponseMessages.ERROR_OCCURRED);

                var storyView = _mapper.Map<StoryView>(story);

                return StandardResponse<StoryView>.Ok(storyView);
            }
            catch (Exception ex)
            {
                return StandardResponse<StoryView>.Failed(ex.Message);
            }
        }

        public StandardResponse<string> DeleteStory(int storyId)
        {
            try
            {
                var loggedOnUser = _httpContextAccessor.HttpContext.User.GetLoggedInUserId<int>();

                var story = _storyRepository.GetById(storyId);

                if (story!= null)
                    _storyRepository.Delete(story);

                return StandardResponse<string>.Ok(StandardResponseMessages.OK);
            }
            catch (Exception ex)
            {
                return StandardResponse<string>.Failed(ex.Message);
            }
        }

        public void DeleteExpiredStories()
        {
            try
            {
                var expiredStories = _storyRepository.ListStories()
                    .Where(story => story.ExpiryDate <= DateTime.Now.AddDays(1));

                _storyRepository.DeleteRange(expiredStories);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }
    }
}