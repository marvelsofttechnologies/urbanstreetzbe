using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using AutoMapper.QueryableExtensions;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using UrbanStreetz.Controllers;
using UrbanStreetz.Models;
using UrbanStreetz.Models.AppModels;
using UrbanStreetz.Models.IdentityModels;
using UrbanStreetz.Models.InputModels;
using UrbanStreetz.Models.UtilityModels;
using UrbanStreetz.Models.ViewModels;
using UrbanStreetz.Repositories.Abstractions;
using UrbanStreetz.Repositories.Interfaces;
using UrbanStreetz.Services.Abstractions;
using UrbanStreetz.Utilities;
using UrbanStreetz.Utilities.Abstrctions;
using UrbanStreetz.Utilities.Constants;
using UrbanStreetz.Utilities.Extentions;

namespace UrbanStreetz.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly ICodeProvider _codeProvider;
        private readonly Globals _appSettings;
        private readonly IEmailHandler _emailHandler;
        private readonly IUserInterestRepository _userInterestRepository;
        private ILocationRepository _locationRepository;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IMediaRepository _mediaRepository;
        private readonly IUserFollowerRepository _userFollowerRepository;
        private readonly IConfigurationProvider _configuration;

        public UserService(UserManager<User> userManager, SignInManager<User> signInManager, IMapper mapper, IUserRepository userRepository, IOptions<Globals> appSettings, IUserInterestRepository userInterestRepository, ILocationRepository locationRepository, IHttpContextAccessor httpContextAccessor, IMediaRepository mediaRepository, ICodeProvider codeProvider, IEmailHandler emailHandler, IUserFollowerRepository userFollowerRepository, IConfigurationProvider configuration)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _mapper = mapper;
            _userRepository = userRepository;
            _appSettings = appSettings.Value;
            _userInterestRepository = userInterestRepository;
            _locationRepository = locationRepository;
            _httpContextAccessor = httpContextAccessor;
            _mediaRepository = mediaRepository;
            _codeProvider = codeProvider;
            _emailHandler = emailHandler;
            _userFollowerRepository = userFollowerRepository;
            _configuration = configuration;
        }

        public StandardResponse<UserView> CreateUser(UserModel model)
        {
            try
            {
                var ExistingUser = _userManager.FindByEmailAsync(model.Email).Result;

                if (ExistingUser != null)
                    return StandardResponse<UserView>.Error(StandardResponseMessages.USER_ALREADY_EXISTS);

                var thisUser = _mapper.Map<User>(model);

                var Result = _userRepository.CreateUser(thisUser).Result;

                if (!Result.Succeeded)
                    return StandardResponse<UserView>.Error(Result.ErrorMessage);

                var createdUser = _userManager.FindByEmailAsync(model.Email).Result;

                foreach (var category in model.Categories)
                {
                    var userIterest = new UserInterest()
                    {
                        UserId = createdUser.Id,
                        PostCategoryId = category
                    };
                    _userInterestRepository.CreateAndReturn(userIterest);
                }

                foreach (var location in model.Locations)
                {
                    var userLocation = _mapper.Map<Location>(location);
                    userLocation.UserId = createdUser.Id;
                    _locationRepository.CreateAndReturn(userLocation);
                }

                var EmailConfirmationToken = _codeProvider.New(Result.CreatedUser.Id, Constants.NEW_EMAIL_VERIFICATION_CODE).CodeString;
                var ConfirmationLink = $"{_appSettings.FrontEndBaseUrl}{_appSettings.EmailVerificationUrl}{EmailConfirmationToken}";

                List<KeyValuePair<string, string>> EmailParameters = new List<KeyValuePair<string, string>>();
                EmailParameters.Add(new KeyValuePair<string, string>(Constants.EMAIL_STRING_REPLACEMENTS_CODE, EmailConfirmationToken.ToUpper()));

                var EmailTemplate = _emailHandler.ComposeFromTemplate(Constants.NEW_USER_WELCOME_EMAIL_FILENAME, EmailParameters);
                var SendEmail = _emailHandler.SendEmail(Result.CreatedUser.Email, Constants.NEW_USER_WELCOME_EMAIL_SUBJECT, EmailTemplate);

                var mapped = _mapper.Map<UserView>(Result.CreatedUser);

                return StandardResponse<UserView>.Ok(mapped);
            }
            catch (Exception ex)
            {
                return StandardResponse<UserView>.Failed(ex.Message);
            }
        }

        public StandardResponse<UserView> AuthenticateAdmin(LoginModel userToLogin)
        {
            try
            {
                var User = _userManager.FindByEmailAsync(userToLogin.Email).Result;
                if (User == null)
                    return StandardResponse<UserView>.Failed().AddStatusMessage(StandardResponseMessages.USER_NOT_FOUND);

                var isInRole = _userManager.IsInRoleAsync(User, "ADMIN").Result;

                var roles = _userManager.GetRolesAsync(User).Result;

                User = _mapper.Map<LoginModel, User>(userToLogin);

                if (!isInRole)
                    return StandardResponse<UserView>.Failed().AddStatusMessage(StandardResponseMessages.USER_NOT_PERMITTED);


                var Result = _userRepository.Authenticate(User).Result;

                if (!Result.Succeeded)
                    return StandardResponse<UserView>.Failed().AddStatusMessage((Result.ErrorMessage ?? StandardResponseMessages.ERROR_OCCURRED));

                var mapped = _mapper.Map<UserView>(Result.LoggedInUser);

                return StandardResponse<UserView>.Ok(mapped);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return StandardResponse<UserView>.Failed();
            }
        }

        public StandardResponse<UserView> VerifyUser(string token, string email)
        {
            try
            {
                var UserToVerify = _userManager.FindByEmailAsync(email).Result;

                Code ThisCode = _codeProvider.GetByCodeString(token.ToLower());

                if (UserToVerify == null)
                    return StandardResponse<UserView>.Failed().AddStatusMessage(StandardResponseMessages.USER_NOT_FOUND);

                if (UserToVerify.EmailConfirmed)
                    return StandardResponse<UserView>.Ok().AddStatusMessage(StandardResponseMessages.ALREADY_ACTIVATED);

                if (ThisCode.IsExpired || ThisCode.ExpiryDate < DateTime.Now || ThisCode.Key != Constants.NEW_EMAIL_VERIFICATION_CODE)
                    return StandardResponse<UserView>.Failed().AddStatusMessage(StandardResponseMessages.EMAIL_VERIFICATION_FAILED);

                UserToVerify.EmailConfirmed = true;
                var Verified = _userManager.UpdateAsync(UserToVerify).Result;

                if (!Verified.Succeeded)
                    return StandardResponse<UserView>.Failed().AddStatusMessage(StandardResponseMessages.ERROR_OCCURRED);

                _codeProvider.SetExpired(ThisCode);

                return StandardResponse<UserView>.Ok().AddStatusMessage(StandardResponseMessages.EMAIL_VERIFIED);
            }
            catch (Exception e)
            {
                Logger.Error(e);
                return StandardResponse<UserView>.Failed();
            }
        }

        public StandardResponse<UserView> Authenticate(LoginModel userToLogin)
        {
            var User = _userManager.FindByEmailAsync(userToLogin.Email).Result;
            if (User == null)
                return StandardResponse<UserView>.Failed().AddStatusMessage(StandardResponseMessages.USER_NOT_FOUND);

            User = _mapper.Map<LoginModel, User>(userToLogin);

            var Result = _userRepository.Authenticate(User).Result;

            if (!Result.Succeeded)
                return StandardResponse<UserView>.Failed().AddStatusMessage((Result.ErrorMessage ?? StandardResponseMessages.ERROR_OCCURRED));

            var mapped = _mapper.Map<UserView>(Result.LoggedInUser);

            return StandardResponse<UserView>.Ok(mapped);
        }

        public StandardResponse<UserView> InitiatePasswordReset(string email)
        {
            try
            {
                var ThisUser = _userManager.FindByEmailAsync(email).Result;

                var Token = _userManager.GeneratePasswordResetTokenAsync(ThisUser).Result;

                Code PasswordResetCode = _codeProvider.New(ThisUser.Id, Constants.PASSWORD_RESET_CODE);

                PasswordResetCode.Token = Token;
                _codeProvider.Update(PasswordResetCode);

                var ConfirmationLink = $"{PasswordResetCode.CodeString}";

                List<KeyValuePair<string, string>> EmailParameters = new List<KeyValuePair<string, string>>();
                EmailParameters.Add(new KeyValuePair<string, string>(Constants.EMAIL_STRING_REPLACEMENTS_URL, ConfirmationLink));
                EmailParameters.Add(new KeyValuePair<string, string>(Constants.EMAIL_STRING_REPLACEMENTS_EXPIRYDATE, PasswordResetCode.ExpiryDate.ToShortDateString()));


                var EmailTemplate = _emailHandler.ComposeFromTemplate(Constants.PASSWORD_RESET_EMAIL_FILENAME, EmailParameters);
                var SendEmail = _emailHandler.SendEmail(ThisUser.Email, Constants.PASSWORD_RESET_EMAIL_SUBJECT, EmailTemplate);

                return StandardResponse<UserView>.Ok().AddStatusMessage(StandardResponseMessages.PASSWORD_RESET_EMAIL_SENT);

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public StandardResponse<UserView> CompletePasswordReset(PasswordReset payload)
        {
            Code ThisCode = _codeProvider.GetByCodeString(payload.Code);

            var ThisUser = _userManager.FindByIdAsync(ThisCode.UserId.ToString()).Result;

            if (ThisUser == null)
                return StandardResponse<UserView>.Failed().AddStatusMessage(StandardResponseMessages.USER_NOT_FOUND);

            if (ThisCode.IsExpired || ThisCode.ExpiryDate < DateTime.Now || ThisCode.Key != Constants.PASSWORD_RESET_CODE)
                return StandardResponse<UserView>.Failed().AddStatusMessage(StandardResponseMessages.PASSWORD_RESET_FAILED);

            var Result = _userManager.ResetPasswordAsync(ThisUser, ThisCode.Token, payload.NewPassword).Result;

            if (!Result.Succeeded)
                return StandardResponse<UserView>.Failed().AddStatusMessage(StandardResponseMessages.ERROR_OCCURRED);

            return StandardResponse<UserView>.Ok().AddStatusMessage(StandardResponseMessages.PASSWORD_RESET_COMPLETE);
        }

        public StandardResponse<UserView> UpdateUser(UpdateUserModel model)
        {
            try
            {
                int UserId = _httpContextAccessor.HttpContext.User.GetLoggedInUserId<int>();
                var thisUser = _userRepository.ListUsers().Result.Users.FirstOrDefault(u => u.Id == UserId);
                if (!string.IsNullOrEmpty(model.PhoneNumber))
                {
                    thisUser.PhoneNumber = model.PhoneNumber;
                    thisUser.Bio1 = model.Bio1;
                    thisUser.Bio2 = model.Bio2;
                    thisUser.Bio3 = model.Bio3;
                    thisUser.WebAddress = model.WebAddress;
                }
                if (model.ProfilePicture != null)
                {
                    var media = _mapper.Map<Media>(model.ProfilePicture);
                    media.Name = thisUser.FirstName + "_" + thisUser.LastName + "_" + "ProfilePicture";

                    var Result = _mediaRepository.UploadMedia(media).Result;

                    if (Result.Succeeded)
                        thisUser.ProfilePictureId = Result.UploadedMedia.Id;
                }

                var up = _userManager.UpdateAsync(thisUser).Result;

                if (!up.Succeeded)
                    return StandardResponse<UserView>.Failed(up.Errors.FirstOrDefault().Description);

                return StandardResponse<UserView>.Ok(_mapper.Map<UserView>(thisUser));
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return StandardResponse<UserView>.Failed();
            }
        }

        public StandardResponse<bool> Follow(int UserId)
        {
            try
            {
                int LoggedInUserId = _httpContextAccessor.HttpContext.User.GetLoggedInUserId<int>();
                UserFollowers newRecord = new()
                {
                    UserId = UserId,
                    FollowerId = LoggedInUserId
                };
                newRecord = _userFollowerRepository.CreateAndReturn(newRecord);

                return StandardResponse<bool>.Ok(true);
            }
            catch (Exception e)
            {
                Logger.Error(e);
                return StandardResponse<bool>.Failed();
            }
        }

        public StandardResponse<bool> UnFollow(int UserId)
        {
            try
            {
                int LoggedInUserId = _httpContextAccessor.HttpContext.User.GetLoggedInUserId<int>();
                var thisFollowingRecord = _userFollowerRepository.GetMyFollowings(LoggedInUserId).FirstOrDefault(f => f.UserId == UserId);

               
                _userFollowerRepository.Delete(thisFollowingRecord);

                return StandardResponse<bool>.Ok(true);
            }
            catch (Exception e)
            {
                Logger.Error(e);
                return StandardResponse<bool>.Failed();
            }
        }

        public StandardResponse<UserProfileView> UserProfile(int userId) {
            try {
                var userProfile = _userRepository.ListUsers().Result.Users
                                                 .FirstOrDefault(user => user.Id == userId);

                if (userProfile == null)
                    return StandardResponse<UserProfileView>.Error(StandardResponseMessages.USER_NOT_FOUND);

                var mappedUserProfile = _mapper.Map<UserProfileView>(userProfile);

                return StandardResponse<UserProfileView>.Ok(mappedUserProfile);
            }
            catch (Exception ex) {
                Logger.Error(ex);

                return StandardResponse<UserProfileView>.Failed(ex.Message);
            }
        }

        public StandardResponse<PagedCollection<UserView>> ListMyFollowers(PagingOptions pagingOptions) {
            try {
                int loggedInUserId = _httpContextAccessor.HttpContext.User.GetLoggedInUserId<int>();

                var followers = _userFollowerRepository.GetUserFollowers(loggedInUserId)
                                                       .Select(follower => follower.User)
                                                       .AsQueryable()
                                                       .ProjectTo<UserView>(_configuration)
                                                       .AsEnumerable();

                var pagedFollowers = followers.Skip(pagingOptions.Offset.Value).Take(pagingOptions.Limit.Value);
                var pagedResponse = PagedCollection<UserView>.Create(
                    Link.ToCollection(nameof(UserController.ListFollowers)),
                    pagedFollowers.ToArray(),
                    followers.Count(),
                    pagingOptions);

                return StandardResponse<PagedCollection<UserView>>.Ok(pagedResponse);
            }
            catch (Exception ex) {
                Logger.Error(ex);

                return StandardResponse<PagedCollection<UserView>>.Failed(ex.Message);
            }
        }
        }
}