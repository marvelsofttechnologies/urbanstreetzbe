using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AspNet.Security.OpenIdConnect.Primitives;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using UrbanStreetz.Models;
using UrbanStreetz.Repositories;
using UrbanStreetz.Repositories.Interfaces;
using UrbanStreetz.Services;
using UrbanStreetz.Services.Interfaces;
using UrbanStreetz.Utilities;
using UrbanStreetz.Utilities.Abstrctions;
using SendGrid.Extensions.DependencyInjection;
using UrbanStreetz.Filters;
using UrbanStreetz.Models.UtilityModels;
using Newtonsoft;
using UrbanStreetz.Models.IdentityModels;
using UrbanStreetz.Context;
using UrbanStreetz.Services.Abstractions;
using UrbanStreetz.Repositories.Abstractions;
using UrbanStreetz.ChatServer.Hubs;

namespace UrbanStreetz
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            var AppSettingsSection = Configuration.GetSection("AppSettings");
            services.Configure<Globals>(AppSettingsSection);
            services.Configure<PagingOptions>(Configuration.GetSection("DefaultPagingOptions"));

            var AppSettings = AppSettingsSection.Get<Globals>();
            var Key = Encoding.ASCII.GetBytes(AppSettings.Secret);

            services.AddSingleton<IConfiguration>(provider => Configuration);

            services.AddDbContext<AppDbContext>(options =>
            {
                options.UseSqlServer(Configuration.GetConnectionString("DbConnect"));
                options.UseOpenIddict<int>();
            });

            //try to initialize npoco so i do not have to pass connection string anywhere in the code
            //services.AddNpoco()


            services.Configure<IdentityOptions>(options =>
            {
                options.Password.RequireDigit = false;
                options.Password.RequireLowercase = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequiredLength = 6;
                options.Password.RequiredUniqueChars = 0;
                options.ClaimsIdentity.UserIdClaimType = OpenIdConnectConstants.Claims.Name;
                options.ClaimsIdentity.UserIdClaimType = OpenIdConnectConstants.Claims.Subject;
                options.ClaimsIdentity.RoleClaimType = OpenIdConnectConstants.Claims.Role;
            });

            services.AddIdentity<User, Role>()
                .AddEntityFrameworkStores<AppDbContext>()
                .AddDefaultTokenProviders();

            services.AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;

                })
                .AddJwtBearer(x =>
                {
                    x.RequireHttpsMetadata = false;
                    x.SaveToken = true;
                    x.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(Key),
                        ValidateIssuer = false,
                        ValidateAudience = false
                    };

                    x.Events = new JwtBearerEvents
                    {
                        OnMessageReceived = context =>
                        {
                            var accessToken = context.Request.Query["access_token"];

                            // If the request is for our hub...
                            var path = context.HttpContext.Request.Path;
                            if (!string.IsNullOrEmpty(accessToken) &&
                                path.StartsWithSegments("/chat"))
                            {
                                // Read the token out of the query string
                                context.Token = accessToken;
                            }
                            return Task.CompletedTask;
                        }
                    };
                });

            
            AddIdentityCoreServices(services);
            //Configure app dependencies
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IMediaRepository,MediaRepository>();
            services.AddTransient<IMediaService,MediaService>();
            services.AddTransient<IEmailHandler, EmailHandler>();
            services.AddTransient<IUtilityMethods,UtilityMethods>();
            services.AddTransient<ICodeProvider, CodeProvider>();
            services.AddTransient<ICountryRepository,CountryRepository>();
            services.AddTransient<ICountryService,CountryService>();
            services.AddTransient<ILocationRepository, LocationRepository>();
            services.AddTransient<IUserInterestRepository,UserInterestRepository>();
            services.AddTransient<IPostService, PostService>();
            services.AddTransient<IPostRepository, PostRepository>();
            services.AddTransient<IUserFollowerRepository,UserFollowerRepository>();
            services.AddTransient<ICommentRepository,CommentRepository>();
            services.AddTransient<ICommentService,CommentService>();
            services.AddTransient<ILikeRepository,LikeRepository>();
            services.AddTransient<IStoryRepository, StoryRepository>();
            services.AddTransient<IStoryService, StoryService>();
            services.AddTransient<IGroupService, GroupService>();
            services.AddTransient<IGroupRepository, GroupRepository>();
            services.AddTransient<IMessageRepository, MessageRepository>();
            services.AddTransient<IMessageService, MessageService>();
            
            services.AddAuthorization();
            services.AddAutoMapper(typeof(Startup));
            services.AddControllers().AddNewtonsoftJson(options =>
                options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            ); 


            
            services.AddMvc(options => {
                options.Filters.Add<LinkRewritingFilter>();
            });
            
            services.AddSwaggerGen(c =>
            {
                c.CustomSchemaIds(type => type.FullName);

                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Urban Streetz", Version = "v1" });

                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Description = "JWT Authorization header using the Bearer scheme (Example: 'Bearer 12345abcdef')",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer"
                });

                c.AddSecurityRequirement(new OpenApiSecurityRequirement {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            }
                        },
                        Array.Empty<string>()
                    }
                });
            });

            services.AddHttpContextAccessor();

            services.AddSignalR();

            services.AddSendGrid(options =>
            {
                options.ApiKey = AppSettings.SendGridApiKey;
            });

            services.AddCors(options =>
            {
                options.AddDefaultPolicy(policy =>
                {
                    policy.AllowAnyHeader();
                    policy.AllowAnyMethod();
                    policy.AllowCredentials();
                });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                
            }
            app.UseSwagger();
            app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Urban Streetz v1"));

            app.UseHttpsRedirection();

            app.UseCors(x => x
                .AllowAnyMethod()
                .AllowAnyHeader()
                .SetIsOriginAllowed(origin => true) // allow any origin
                .AllowCredentials());

            app.UseRouting();

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHub<ChatHub>("/chat");
                endpoints.MapControllers();
            });
        }

        private static void AddIdentityCoreServices(IServiceCollection services)
        {
            var builder = services.AddIdentityCore<User>();
            builder = new IdentityBuilder(
                builder.UserType,
                typeof(Role),
                builder.Services
            );

            builder.AddRoles<Role>()
                .AddEntityFrameworkStores<AppDbContext>()
                .AddDefaultTokenProviders()
                .AddSignInManager<SignInManager<User>>();
        }
    }
}
