﻿
using System.Linq;

using AutoMapper;
using UrbanStreetz.Models.AppModels;
using UrbanStreetz.Models.IdentityModels;
using UrbanStreetz.Models.InputModels;
using UrbanStreetz.Models.ViewModels;

namespace UrbanStreetz.Utilities {
    public class AutoMapperConfig : Profile {
        public AutoMapperConfig( ) {
            CreateMap<LocationModel, Location>();
            CreateMap<UserModel, User>();
            CreateMap<User, UserView>();
            CreateMap<UserView, User>();
            CreateMap<Country, CountryView>();
            CreateMap<State, StateView>();
            CreateMap<City, BaseNameModelView>();
            CreateMap<PostModel, Post>();

            CreateMap<Post, PostView>()
               .ForMember(dest => dest.Status, opt => opt.MapFrom(src =>
                    src.Status != null ? src.Status.Name : ""))
               .ForMember(dest => dest.PostCategory, opt => opt.MapFrom(src =>
                    src.PostCategory!= null ? src.PostCategory.Name : ""));

            CreateMap<PostCategory, PostCategoryView>();

            CreateMap<LoginModel, User>()
                .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.Email));

            CreateMap<MediaModel, Media>();
            CreateMap<StoryModel, Story>();

            CreateMap<Story, StoryView>()
                .ForMember(dest => dest.MediaUrl, opt => opt.MapFrom(src => 
                    src.Media != null ? src.Media.Url : ""))
               .ForMember(dest => dest.FullName, opt => opt.MapFrom(src =>
                                                                        src.User != null
                                                                            ? src.User.FullName
                                                                            : ""));
            CreateMap<StoryGroup, StoryGroupView>();

            CreateMap<GroupModel, Group>();
            CreateMap<Group, GroupView>()
                .ForMember(dest => dest.CreatorName, opt => opt.MapFrom(src =>
                    src.CreatorUser.FullName));
            CreateMap<MessageModel, Message>();
            CreateMap<Message, MessageView>()
                .ForMember(dest => dest.SenderName, opt => opt.MapFrom(src =>
                    src.Sender.FullName))
                .ForMember(dest => dest.RecipientName, opt => opt.MapFrom(src =>
                    src.Recipient != null
                        ? src.Recipient.FullName
                        : null))
                .ForMember(dest => dest.ParentText, opt => opt.MapFrom(src =>
                    src.Parent != null
                        ? src.Parent.Text
                        : null));
                //.ForMember(dest => dest.GroupName, opt => opt.MapFrom(src =>
                //    src.Group != null
                //        ? src.Group.Name
                //        : null))

            CreateMap<Media, MediaView>();

            CreateMap<User, UserProfileView>()
               .ForMember(dest => dest.ProfilePictureUrl, opt => opt.MapFrom(src => src.ProfilePicture != null
                                                                                 ? src.ProfilePicture.Url
                                                                                 : null))
               .ForMember(dest => dest.NoOfFollowers, opt => opt.MapFrom(src => src.Followers.Count()))
               .ForMember(dest => dest.NoFollowing, opt => opt.MapFrom(src => src.Following.Count()))
               .ForMember(dest => dest.NoOfPosts, opt => opt.MapFrom(src => src.Posts.Count()));

            CreateMap<CommentModel, Comment>();
            CreateMap<Comment, CommentView>()
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src =>
                    src.Status != null ? src.Status.Name : ""));

            CreateMap<HighlightModel, Highlight>();
            CreateMap<Highlight, HighlightView>()
               .ForMember(dest => dest.CreatorUser, opt => opt.MapFrom(src => src.CreatorUser.FullName));

            CreateMap<CartModel, Cart>();

            CreateMap<Cart, CartView>()
               .ForMember(dest => dest.PostFeed, opt => opt.MapFrom(src => src.Post.Feed))
               .ForMember(dest => dest.PostDescription, opt => opt.MapFrom(src => src.Post.Description))
               .ForMember(dest => dest.PostBrand, opt => opt.MapFrom(src => src.Post.Brand))
               .ForMember(dest => dest.PostSize, opt => opt.MapFrom(src => src.Post.Size));
        }
    }
}
