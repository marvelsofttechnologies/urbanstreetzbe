﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UrbanStreetz.Context;
using UrbanStreetz.Models.AppModels;
using UrbanStreetz.Repositories;
using UrbanStreetz.Repositories.Interfaces;
using UrbanStreetz.Utilities.Abstrctions;

namespace UrbanStreetz.Utilities
{
    public class CodeProvider : BaseRepository<Code>, ICodeProvider
    {
        private readonly IUtilityMethods _utilityMethods;

        public CodeProvider(AppDbContext context, IUtilityMethods utilityMethods) : base(context)
        {
            _utilityMethods = utilityMethods;
        }

        public Code New(int userId, string key, int expiryInMinutes = 2880, int length = 6, string prefix = "", string suffix = "")
        {
            try
            {
                var NewCode = new Code
                {
                    UserId = userId <= 0 ? null : userId,
                    Key = key ?? "",
                    CodeString = prefix.ToLower() + _utilityMethods.RandomCode(length).ToLower() + suffix.ToLower(),
                    ExpiryDate = DateTime.Now.AddMinutes(expiryInMinutes),
                    DateCreated = DateTime.Now,
                    DateModified = DateTime.Now
                };
                while ((GetByCodeString(NewCode.CodeString) != null) || NewCode.CodeString == null)
                {
                    NewCode.CodeString = prefix + _utilityMethods.RandomCode(length) + suffix;
                }

                NewCode = CreateAndReturn(NewCode);

                return NewCode;


            }
            catch (Exception e)
            {
                Logger.Error(e);
                return null;
            }
        }

        public Code GetByCodeString(string code)
        {
            return GetAll().FirstOrDefault(c => c.CodeString == code);
        }

        public IEnumerable<Code> GetByUserId(int Id)
        {
            var codes = ToList();
            var userCodes = codes.Where(c => c.UserId == Id);
            return userCodes;
        } 

        public bool SetExpired(Code code)
        {
            try
            {
                code.ExpiryDate = DateTime.Now;
                code.IsExpired = true;
                Update(code);
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e);
                return true;
            }
        }

        // public bool Update(Code thisCode)
        // {
        //     try
        //     {
        //         Update(thisCode);
        //         return true;
        //     }
        //     catch(Exception e)
        //     {
        //         Logger.Error(e);
        //         return false;
        //     }
        // }
    }
}
