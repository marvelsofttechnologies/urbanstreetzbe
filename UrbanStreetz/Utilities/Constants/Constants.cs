﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UrbanStreetz.Utilities.Constants
{
    public class Constants
    {
        public const string NEW_EMAIL_VERIFICATION_CODE = "Email Verification Token";
        public const string NEW_USER_WELCOME_EMAIL_SUBJECT = "Welcome to Alvin Grey";
        public const string NEW_USER_REVERIFY_EMAIL_SUBJECT = "Please verify your account";
        public const string EMAIL_STRING_REPLACEMENTS_URL = "URL";
        public const string EMAIL_STRING_REPLACEMENTS_CODE = "CODE";
        public const string NEW_USER_WELCOME_EMAIL_FILENAME = "new-user.html";
        public const string NEW_USER_REVERIFY_EMAIL_FILENAME = "reverify.html";
        public const string PASSWORD_RESET_CODE = "Password reset token";
        public const string PASSWORD_RESET_EMAIL_SUBJECT = "Password Reset";
        public const string PASSWORD_RESET_EMAIL_FILENAME = "password-reset.html";
        public const string LOGIN_CREDENTIALS_INCORRECT = "Email or Password is incorrect.";
        public const string NEW_PROPERTY_MEDIA_NAME = "New Unique Code for Media uplaod";
        public const string PROPERTY_MATAAZ_MEDIA_PREFIX = "pm_media";
        public static string EMAIL_STRING_REPLACEMENTS_EXPIRYDATE = "EXPIRYDATE";
        public static string USER_REFFERAL_CODE = "REFFERAL CODE";
    }
}
