﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UrbanStreetz.Utilities
{
    public class StandardResponseMessages
    {
        public static string OK => SUCCESSFUL;
        public static string PASSWORD_RESET_EMAIL_SENT = "An email has been sent to you with instructions and next steps";
        public static string SUCCESSFUL = "Successful";
        public static string  USER_NOT_PERMITTED = "Sorry you are not permitted to log in as an administrator";
        public static string UNSUCCESSFUL = "Unsuccessful";
        public static string ERROR_OCCURRED = "An Error Occurred, please try again later";
        public static string EMAIL_VERIFIED = "Email verification successful";
        public static string ALREADY_ACTIVATED = "Email already verified";
        public static string EMAIL_VERIFICATION_FAILED =
            "Email verification failed. This Link may have expired please contact admin";
        public static string USER_NOT_FOUND = "User with this email does not exist";
        public static string PASSWORD_RESET_FAILED =
            "Password reset failed. This Link may have expired please contact admin";
        public static string PASSWORD_RESET_COMPLETE = "Your password has been reset successfully";
        public static string USER_ALREADY_EXISTS = "A user with this email already exists.";
        public static string PROPERTY_CREATION_SUCCESSFUL = "Your new Property was created successfully";
        public static string PROPERTY_CREATION_FAILED = "There was an error while creating your property, please try again after sometime. If this persists please contact admin";
        public static string MEDIA_UPLOAD_FAILED = "We encountered an error uploading this media at this time";
        public static string MEDIA_UPLOAD_SUCCESSFUL = "Media Uploaded successfully";
        internal static string DEELETED;
        public static string NOT_FOUND = "The resource you requested for was not found.";
        public static string HIGHLIGHT_NAME_INVALID = "Invalid name provided for hightlight.";
    }
}
